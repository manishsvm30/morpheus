#include "string_functions.h"



namespace str {

std::vector<string> split(const string& val, const string& chars, SplitBehavior empty_parts) {
	std::vector<string> result;
	if (empty_parts == SkipEmptyParts) {
		boost::split(result, val, boost::is_any_of(chars), boost::token_compress_on );
		result.erase ( std::remove_if(result.begin(), result.end(), [](const string& v) { return v.empty(); } ), result.end());
	}
	else {
		boost::split(result, val, boost::is_any_of(chars), boost::token_compress_off );
	}
	return result;
}

string& arg(string& val, const string& repl) {
	std::map< int, std::pair<int,int> > placeholders;
	std::smatch match;
	string current = val;
	while ( std::regex_search(current,match,std::regex("%(\\d+)")) ) {
		placeholders.insert({std::atoi(match.str(1).c_str()), {match.position(), match.length()}});
		current = match.suffix();
	}
	if (!placeholders.empty()) {
		const auto& r=placeholders.begin()->second;
		val.replace(r.first, r.second, repl);
	}
	return val;
}

bool contains(const string& val, const string& sub) {
	return val.find(sub) != string::npos;
}

bool startsWith(const string& val, const string& sub) { 
	return val.compare(0,sub.size(), sub) == 0;
}

bool endsWith(const string& val, const string& sub) { 
	return val.compare(val.size()-sub.size(),sub.size(), sub) == 0;
}

string& remove_all_of(string& val, const string& chars) {
	
	auto pos = val.find_first_of(chars);
	while ( pos!=string::npos ) {
		val.erase(pos);
		pos = val.find_first_of(chars, pos);
	}
	return val;
};
	
string& remove(string& val, const string& substr) {
	string::size_type pos = 0;
	
	while ( ( pos = val.find(substr,0)) != string::npos ) {
		val.erase(pos,substr.size());
	}
	return val;
};
	
}; // namespace str



	
	
std::istream& operator >> (std::istream & stream, deque<double>& q){
	double tmp;
	int nread(0);

	while (1) {
		stream >> tmp;
		if ( stream.eof() ||  ! stream.fail() ) break;
		q.push_back(tmp);
		nread++;
	}
	return stream;
}
std::ostream& operator << (std::ostream & stream, const deque<double>& q) {
	copy(q.begin(), q.end(), std::ostream_iterator<double>(stream," "));
	return stream;
}


std::ostream& operator << (std::ostream& out, bool b) {
	if (b) out << "true";
	else out << "false";
	return out;
}

std::istream& operator >> (std::istream& in, bool& b){
	string val; char c;
	while (! in.fail()) {
		in.get(c);
		if ( in.fail()  || !isalnum(c)) break;
		val.push_back(c);
	}
	if (val.empty()) return in;
	if (val =="1")  { b=true; return in; }
	if (val =="0")  { b=false; return in; }
	lower_case(val);
	if ( val=="true") b= true;
	else b=false;
	return in;
}


// string to_str(int value, int width) {
// 	static string s;
// 	s.resize()
// 	snprintf()
// 	static bool init=false;
// 	if (!init)  { s.fill('0'); init=true; }
// 	else  { s.clear(); s.str("");}
// 	if (width>=0)
// 		s.width(width);
// 	else 
// 		s.width(0);
// 	s << value; 
// 	return s.str();
// }

/*string to_str(uint value, int width) {
	static stringstream s("");
	static bool init=false;
	if (!init)  { s.fill('0'); init=true; }
	else  { s.clear(); s.str("");}
	if (width>=0)
		s.width(width);
	else 
		s.width(0);
	s << value; 
	return s.str();
}*/
/*
string to_str(bool value, int prec) {
	if (value) return "true";
	return "false";
}*/

string strip_last_token(string& s, const string& del) {
	string token;
	string::size_type pos = s.find_last_of(del);
	if (pos == string::npos) {
		token = s;
		s="";
	}
	else {
		pos++;
		token = s.substr(pos);
		s.resize(pos-1);
	}
	return token;
}

string remove_spaces(string& str){
	str.erase (std::remove (str.begin(), str.end(), ' '), str.end());
	return str;
}

string replace_spaces(string& str, char replacement){
    std::replace(str.begin(), str.end(), ' ', replacement);
    return str;
}

bool replace_substring(std::string& str, const std::string& from, const std::string& to) {
    size_t start_pos = str.find(from);
    if(start_pos == std::string::npos)
        return false;
    str.replace(start_pos, from.length(), to);
    return true;
}



// vector<string> split_string(const string& str, const string& delimiters = " ");
vector<string> tokenize(const string& str, const string& delimiters, bool drop_empty_tokens) {

	vector<string> tokens;
	string::size_type fPos=0,lPos=0;
	do {
		// Skip delimiters at beginning.
		fPos = str.find_first_not_of(delimiters, lPos);
		// Don't add empty strings
		if (fPos == string::npos) break;
		// Iterate until first delimiter
		lPos = str.find_first_of(delimiters, fPos);
		// eat all the string left, if no further delimiter was found
		if (lPos == string::npos) lPos = str.length();
		// Add the token to the vector.
		if (!drop_empty_tokens || lPos>fPos) tokens.push_back(str.substr(fPos, lPos - fPos));
	} while (lPos != str.length());

	return tokens;
}

// string join(const vector<string>& strings, string delim) {
// 	string res;
// 	if (strings.size()==0)
// 		return res;
// 	res = strings.front();
//     for (int i=1; i<strings.size() ;i++) {
// 		res.append(delim).append(strings[i]);
// 	}
// 	return res;
// }
// 
// string join(const set<string>& strings, string delim) {
// 	string res;
// 	if (strings.size()==0)
// 		return res;
// 	res = *strings.begin();
// 	for (set<string>::const_iterator i=++strings.begin(); i!=strings.end(); i++) {
// 		res.append(delim).append(*i);
// 	}
// 	return res;
// }
// 
// string join(const multiset<string>& strings, string delim) {
// 	string res;
// 	if (strings.size()==0)
// 		return res;
// 	res = *strings.begin();
// 	for (set<string>::const_iterator i=++strings.begin(); i!=strings.end(); i++) {
// 		res.append(delim).append(*i);
// 	}
// 	return res;
// }

string& lower_case(string& a) {
	transform(a.begin(), a.end(), a.begin(), (int(*)(int))std::tolower);
	return a;
}

string lower_case(const char* a) {
	string l(a);
	lower_case(l);
	return l;
}
