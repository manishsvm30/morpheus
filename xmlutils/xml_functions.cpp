#include "xml_functions.h"

string readFile(string filename) {
	gzFile file=gzopen(filename.c_str(), "rb");
	if (file==NULL) { cerr << "unable to open file " << filename << endl; exit(-1); }
	cout << "Initializing from file " << filename << endl;
	string stringbuff;	int error;
	char* buff= (char*)malloc(16384*sizeof(char));
	int i;
	while ((i=gzread(file,buff,sizeof(buff)))>0) {
		buff[i]='\0';
		stringbuff+=buff;
	}
	free(buff);
	gzerror(file,&error);
	gzclose(file); 
	if (!(error==Z_STREAM_END or error==Z_OK)) {
		cerr << "Error " << error << " while reading " << filename << ": "<< gzerror(file,&error);
		return "";
	}
	return stringbuff;
}

XMLNode parseXMLFile(string filename, XMLResults *pResults) {
	
// and parse the file
	cout << "parsing ..." << endl;
	XMLNode xml_root = XMLNode::parseString(readFile(filename).c_str(),"MorpheusModel", pResults);
	
	return xml_root;
}

XMLNode getXMLNode(const XMLNode XML_base, const string& path) {
	vector<string> tags=tokenize(path,"/");
	
	XMLNode xNode = XML_base;
	for (vector<string>::iterator itag=tags.begin(); itag<tags.end();itag++) {
		if ( itag->empty() ) continue;
		int ntag=0;
		string::size_type r_bracket, l_bracket;
		// select from multiple identical tags via indexing --> [i]
		if ((r_bracket=itag->find_last_of("]")) != string::npos && (l_bracket = itag->find_last_of("[")) != string::npos && r_bracket > l_bracket)  {
			std::stringstream tmp(itag->substr(l_bracket+1, r_bracket-1));
			itag->resize(l_bracket);
			tmp >> ntag;
//			cout << "[" << *itag << " "<< ntag+1 << "of"<< xNode.nChildNode(itag->c_str()) << "]"; 
		}
		if ( xNode.nChildNode(itag->c_str()) > ntag ) xNode = xNode.getChildNode(itag->c_str(),ntag); 
		else
			return XMLNode::emptyXMLNode;
	}
	return xNode;
}

string getXMLPath(const XMLNode node) {
	if (node.isEmpty())
		return "";
	const XMLNode parent = node.getParentNode();
	if (parent.isEmpty())
		return node.getName() ? node.getName() : string();

	string path;
	path = getXMLPath(parent);
	
	int n_childnodes = parent.nChildNode(node.getName());
	
	if (n_childnodes>1) {
		// let's find the position of the current node
		int pos;
		for (pos=0; pos<n_childnodes; pos++) {
			if ( parent.positionOfChildNode(parent.getChildNode(node.getName(),pos)) == parent.positionOfChildNode(node)) 
				break;
		}
		if (pos == n_childnodes) {
			std::cerr << "getXMLPath Ooops: Cannot find XML node in the parent" << endl;
		}
		path += string("/") + node.getName() + "[" + to_str(pos) + "]";
	}
	else {
		path += string("/") + node.getName();
	}
	return path;
}

int getXMLnNodes(const XMLNode XML_base, string path) {
	string last_tag = strip_last_token(path,"/");
	XMLNode xNode=getXMLNode(XML_base, path);
	return xNode.nChildNode(last_tag.c_str());
}

bool setXMLAttribute(XMLNode XML_base, const string& path, const string& value) {
	return setXMLAttribute(XML_base, path, value.c_str());
}

bool setXMLAttribute(XMLNode XML_base, string path, const char* value) {
	string attribute=strip_last_token(path,"/");
	if (attribute.empty()) return false;

	XMLNode xNode = getXMLNode(XML_base,path);
	if (xNode.isEmpty()) return false;
	if (lower_case(attribute) == "text") {
		xNode.updateText(value,0);
	}
	else xNode.updateAttribute( value, attribute.c_str(), attribute.c_str() );
	return true;
}

namespace xml {
}

