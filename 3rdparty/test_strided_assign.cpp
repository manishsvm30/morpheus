#define strided_row_major_fallback
#define strided_parallel_assign

#include "xtensor/xtensor.hpp"
#include "xtensor/xassign.hpp"
#include "xtensor/xnoalias.hpp"
#include "xtensor/xview.hpp"
#include "xtensor/xstrided_view.hpp"
#include <chrono>
#include <string>
#include <cstdio>
#include <iomanip>
#include <omp.h>



const int kernel_repetitions = 5;
const int size_scale = 500;
const std::array<size_t,3> shape = {size_scale,size_scale,size_scale};
xt::xtensor<double,3> a(shape), b(shape);


void reset() {
	b.fill(0);
}

template <class T>
void perform_test(const T& callable_kernel, std::string kernel_name, int threads) {
	reset();
	std::chrono::high_resolution_clock highc;
	auto start = highc.now();
	timespec cstart; clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cstart);
	
	omp_set_num_threads(threads);
	for (int i=0; i<kernel_repetitions; i++ ) {
		callable_kernel();
	}

	timespec cstop; clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cstop);
	double d_cputime = (cstop.tv_sec - cstart.tv_sec) * 1e3 /*+ (cstop.tv_nsec-cstart.tv_nsec) * 1e-6*/;
	double d_runtime = std::chrono::duration_cast<std::chrono::milliseconds>(highc.now()-start).count();
	auto checksum = xt::sum(b)();
	std::cout << "Kernel '" << kernel_name << "' took " << d_runtime << " ms; checksum " << std::setprecision(9)<< checksum << std::endl;
	
	
}


int main(int argn, char** argc )  {
	
	a.fill(0);
	auto core = xt::range(1,size_scale-1);
	xt::noalias(xt::view(a, core, core, core)) = 1;
	
	perform_test([](){ xt::noalias(b)=a; }, "plain assign", 1);
	perform_test([](){ xt::noalias(b)=a; }, "plain assign", 4);
	
	perform_test([](){ xt::noalias(b)= 1*a; }, "expression assign", 1);
	perform_test([](){ xt::noalias(b)= 1*a; }, "expression assign", 4);
	
	perform_test([&](){ xt::noalias(xt::view(b, xt::all(), xt::all(), xt::all())) = xt::view(a, xt::all(), xt::all(), xt::all()); }, "range assign", 1);
	perform_test([&](){ xt::noalias(xt::view(b, xt::all(), xt::all(), xt::all())) = xt::view(a, xt::all(), xt::all(), xt::all()); }, "range assign", 4);
	
	perform_test([&](){ xt::noalias(xt::view(b, core, core, core)) = xt::view(a, core, core, core); }, "sub-range assign", 1);
	perform_test([&](){ xt::noalias(xt::view(b, core, core, core)) = xt::view(a, core, core, core); }, "sub-range assign", 4);
	
	auto kernel_assign = [&](){ 
		xt::noalias(xt::view(b, xt::all(), xt::all(), xt::all())) 
			= (  xt::view(a, xt::all(), xt::all(), xt::all())
			  + 1 * xt::view(a, xt::all(), xt::all(), xt::all())
			  + 1 * xt::view(a, xt::all(), xt::all(), xt::all()) 
			  + 1 * xt::view(a, xt::all(), xt::all(), xt::all())
			  + 1 * xt::view(a, xt::all(), xt::all(), xt::all())
			  + 1 * xt::view(a, xt::all(), xt::all(), xt::all())
			  + 1 * xt::view(a, xt::all(), xt::all(), xt::all())); 
	};
	perform_test(kernel_assign, "kernel assign", 1);
	perform_test(kernel_assign, "kernel assign", 4);

	auto kernel_strided_sub_range_assign = [&](){ 
		xt::noalias(xt::strided_view(b,{core, core, core}))
			= (  xt::strided_view(a, {core, core, core})
			  + 1 * xt::strided_view(a, {core, core, xt::range(2,size_scale)})
			  + 1 * xt::strided_view(a, {core, core, xt::range(0,size_scale-2)})
			  + 1 * xt::strided_view(a, {core, xt::range(2,size_scale), core})
			  + 1 * xt::strided_view(a, {core, xt::range(0,size_scale-2), core}) 
			  + 1 * xt::strided_view(a, {xt::range(2,size_scale), core, core}) 
			  + 1 * xt::strided_view(a, {xt::range(0,size_scale-2), core, core}) ); 
	};
	perform_test(kernel_strided_sub_range_assign, "kernel strided_view sub-range assign", 1);
	perform_test(kernel_strided_sub_range_assign, "kernel strided_view sub-range assign", 4);
	
	auto kernel_sub_range_assign = [&](){ 
		xt::noalias(xt::view(b,core, core, core))
			= (  xt::view(a, core, core, core)
			  + 1 * xt::view(a, core, core, xt::range(2,size_scale))
			  + 1 * xt::view(a, core, core, xt::range(0,size_scale-2))
			  + 1 * xt::view(a, core, xt::range(2,size_scale), core)
			  + 1 * xt::view(a, core, xt::range(0,size_scale-2), core) 
			  + 1 * xt::view(a, xt::range(2,size_scale), core, core) 
			  + 1 * xt::view(a, xt::range(0,size_scale-2), core, core) ); 
	};
	perform_test(kernel_sub_range_assign, "kernel view sub-range assign", 1);
	perform_test(kernel_sub_range_assign, "kernel view sub-range assign", 4);
	
	auto manual_kernel_sub_range_assign = [&](){ 
		auto fl_a = xt::flatten(a);
		auto fl_b = xt::flatten(b);
		auto x_o = 1;
		auto y_o = size_scale;
		auto z_o = size_scale * size_scale;
#pragma omp parallel for
		for (size_t z=1; z<size_scale-1; z++ ) {
			for (size_t y=1; y<size_scale-1; y++ ) {
				auto o = y*y_o + z*z_o;
				for (size_t x=1; x<size_scale-1; x++ ) {
					b[x+o]  = a[x+o] + a[x+o-x_o] +a[x+o+x_o] + a[x+o-y_o] +a[x+o+y_o] + a[x+o-z_o] +a[x+o+z_o];
				}
			}
		}
	};
	perform_test(manual_kernel_sub_range_assign, "manual kernel view sub-range assign", 1);
	perform_test(manual_kernel_sub_range_assign, "manual kernel view sub-range assign", 4);
	return 0;
}
