<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Title>Example-CellCycle-MultiScale</Title>
        <Details>- NOTE: cell Property 'dist' does not give correct values (see Logger): values are identical for all cells!


ODE model of Xenopus oocyte cell cycle adopted from:

James Ferrell, Tony Yu-Chen Tsai and Qiong Yang (2011) Modeling the Cell Cycle: Why Do Certain Circuits Oscillate?, Cell 144, p874-885. http://dx.doi.org/10.1016/j.cell.2011.03.006</Details>
    </Description>
    <Global>
        <Field symbol="g" value="0">
            <Diffusion rate="100"/>
        </Field>
        <System solver="Runge-Kutta [fixed, O(4)]" time-step="0.1">
            <DiffEqn symbol-ref="g">
                <Expression>APC - 0.05*g</Expression>
            </DiffEqn>
        </System>
        <Constant symbol="CDK1" value="0.0"/>
        <Constant symbol="APC" value="0.0"/>
        <Constant symbol="Plk1" value="0.0"/>
        <Constant symbol="dist" value="0.0"/>
        <Constant symbol="posx" value="0.0"/>
    </Global>
    <Space>
        <Lattice class="hexagonal">
            <Size symbol="size" value="250, 250, 0"/>
            <BoundaryConditions>
                <Condition boundary="x" type="periodic"/>
                <Condition boundary="y" type="periodic"/>
            </BoundaryConditions>
            <Neighborhood>
                <Order>1</Order>
            </Neighborhood>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="2"/>
        <TimeSymbol symbol="t"/>
        <RandomSeed value="3445"/>
    </Time>
    <CellTypes>
        <CellType name="cells" class="biological">
            <Property name="Cyclin-dependent kinase 1" symbol="CDK1" value="0"/>
            <Property name="Polo-like kinase 1" symbol="Plk1" value="0"/>
            <Property name="Anaphase-promoting complex" symbol="APC" value="0"/>
            <System time-scaling="15" solver="Dormand-Prince [adaptive, O(5)]">
                <DiffEqn symbol-ref="CDK1">
                    <Expression>α1+(g_l) - β1 * CDK1 * (APC^n / (K^n + APC^n))</Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="Plk1">
                    <Expression>α2*(1-Plk1) * ((CDK1^n) / (K^n + CDK1^n)) - β2*Plk1</Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="APC">
                    <Expression>α3*(1- APC) * ((Plk1^n) / (K^n + Plk1^n)) - β3*APC</Expression>
                </DiffEqn>
                <Constant name="Hill coefficient" symbol="n" value="8"/>
                <Constant name="Michaelis constant" symbol="K" value="0.5"/>
                <Constant symbol="α1" value="0.1"/>
                <Constant symbol="α2" value="3.0"/>
                <Constant symbol="α3" value="3.0"/>
                <Constant symbol="β1" value="3.0"/>
                <Constant symbol="β2" value="1.0"/>
                <Constant symbol="β3" value="1.0"/>
            </System>
            <Property name="divisions" symbol="d" value="0"/>
            <Property name="Target volume" symbol="Vt" value="25000"/>
            <VolumeConstraint strength="1" target="Vt"/>
            <SurfaceConstraint strength="0.5" target="1.0" mode="aspherity"/>
            <Property symbol="g_l" value="0.0"/>
            <CellDivision division-plane="minor" trigger="on-change">
                <Condition>CDK1 > 0.5 and cell.volume > 100</Condition>
                <Triggers>
                    <Rule symbol-ref="d">
                        <Expression>d+0.5</Expression>
                    </Rule>
                    <Rule symbol-ref="Vt">
                        <Expression>Vt/2</Expression>
                    </Rule>
                </Triggers>
            </CellDivision>
            <Property symbol="dist" value="sqrt( (cell.center.x-size.x/2)^2 + (cell.center.y-size.y/2)^2 )  "/>
            <Mapper name="report field">
                <Input value="g"/>
                <Output symbol-ref="g_l" mapping="average"/>
            </Mapper>
        </CellType>
        <CellType name="Medium" class="medium">
            <Property name="Anaphase-promoting complex" symbol="APC" value="0"/>
        </CellType>
    </CellTypes>
    <CPM>
        <Interaction default="0">
            <Contact type1="cells" value="-12" type2="cells"/>
            <Contact type1="cells" value="0" type2="Medium"/>
        </Interaction>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="1e-4"/>
            <Neighborhood>
                <Order>1</Order>
            </Neighborhood>
            <MetropolisKinetics temperature="2" yield="0.1"/>
        </MonteCarloSampler>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Order>3</Order>
            </Neighborhood>
        </ShapeSurface>
    </CPM>
    <CellPopulations>
        <Population type="cells" size="0">
            <InitCellObjects mode="order">
                <Arrangement repetitions="1, 1, 1" displacements="1, 1, 1">
                    <Sphere radius="45" center="125, 110, 0"/>
                </Arrangement>
            </InitCellObjects>
            <InitProperty symbol-ref="CDK1">
                <Expression>0.25</Expression>
            </InitProperty>
        </Population>
    </CellPopulations>
    <Analysis>
        <Gnuplotter time-step="0.1" decorate="false">
            <Terminal name="png"/>
            <Plot>
                <Cells value="APC" opacity="0.5">
                    <ColorMap>
                        <Color color="blue" value="0.5"/>
                        <Color color="white" value="0.25"/>
                    </ColorMap>
                </Cells>
                <Field isolines="5" symbol-ref="g" coarsening="2" min="0.0">
                    <ColorMap>
                        <Color color="red" value="1.0"/>
                        <Color color="yellow" value="0.5"/>
                        <Color color="white" value="0.0"/>
                    </ColorMap>
                </Field>
            </Plot>
        </Gnuplotter>
        <Logger time-step="0.002">
            <Input>
                <!--    <Disabled>
        <Symbol symbol-ref="APC"/>
    </Disabled>
-->
                <!--    <Disabled>
        <Symbol symbol-ref="CDK1"/>
    </Disabled>
-->
                <!--    <Disabled>
        <Symbol symbol-ref="Plk1"/>
    </Disabled>
-->
                <Symbol symbol-ref="dist"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <!--    <Disabled>
        <Plots>
            <Plot time-step="0.25">
                <Style style="points"/>
                <Terminal terminal="png"/>
                <X-axis>
                    <Symbol symbol-ref="t"/>
                </X-axis>
                <Y-axis>
                    <Symbol symbol-ref="APC"/>
                </Y-axis>
                <Color-bar>
                    <Symbol symbol-ref="dist"/>
                </Color-bar>
            </Plot>
        </Plots>
    </Disabled>
-->
        </Logger>
        <ModelGraph include-tags="#untagged" format="svg" reduced="false"/>
    </Analysis>
</MorpheusModel>
