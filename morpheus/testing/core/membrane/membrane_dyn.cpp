
#include "gtest/gtest.h"
#include "model_test.h"
#include "core/simulation.h"
#include "core/focusrange.h"

const double mass_error_tolerance_factor = 1e-8;
const double operator_error_tolerance_factor = 2e-3;

TEST (Membrane, PCP) {
	
	auto file1 = ImportFile("MembranePCP_4x4_and_mass_conservation.xml");
	auto model = TestModel(file1.getDataAsString());

	model.run();
	
	auto alignment = SIM::findGlobalSymbol<double>("Test.PCP") -> get(SymbolFocus::global);
	auto mass_dev = SIM::findGlobalSymbol<double>("Test.mass") -> get(SymbolFocus::global);
	
	EXPECT_LE(alignment, 1e-2);
	EXPECT_LE(mass_dev, 1e-10);
}
