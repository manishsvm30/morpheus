
#include "gtest/gtest.h"
#include "model_test.h"
#include "core/simulation.h"
#include "core/focusrange.h"

const double mass_error_tolerance_factor = 1e-8;
const double operator_error_tolerance_factor = 2e-3;

TEST (FieldDiffusion, Square) {
	
	auto file1 = ImportFile("field_diffusion_001.xml");
	auto model = TestModel(file1.getDataAsString());

	model.run();
	
	auto field = SIM::findGlobalSymbol<double>("f");
	
	auto initial_mass = SIM::findGlobalSymbol<double>("initial_mass") -> get(SymbolFocus::global);
	auto mass = SIM::findGlobalSymbol<double>("mass") -> get(SymbolFocus::global);
	EXPECT_NEAR(mass, initial_mass, initial_mass*mass_error_tolerance_factor);
	
	auto total_error = SIM::findGlobalSymbol<double>("total_error") -> get(SymbolFocus::global);
	EXPECT_NEAR(total_error/initial_mass, 0, operator_error_tolerance_factor);
}

TEST (FieldDiffusion, SquareDomain) {
	
	auto file1 = ImportFile("field_diffusion_004.xml");
	auto model = TestModel(file1.getDataAsString());

	model.run();
	
	auto field = SIM::findGlobalSymbol<double>("f");
	
	auto initial_mass = SIM::findGlobalSymbol<double>("initial_mass") -> get(SymbolFocus::global);
	auto mass = SIM::findGlobalSymbol<double>("mass") -> get(SymbolFocus::global);
	EXPECT_NEAR(mass, initial_mass, initial_mass*mass_error_tolerance_factor);
	
	auto total_error = SIM::findGlobalSymbol<double>("total_error") -> get(SymbolFocus::global);
	EXPECT_NEAR(total_error/initial_mass, 0, operator_error_tolerance_factor);
}

TEST (FieldBoundary, Square) {
	
	auto file1 = ImportFile("field_flux_boundary_square.xml");
	auto model = TestModel(file1.getDataAsString());

	// heterogeneous heterogeneous
	model.setParam("D","Df");
	model.run();
	for (auto err_fun : {"fx_error", "fy_error", "cx_error", "cy_error" }) {
		auto error = SIM::findGlobalSymbol<double>(err_fun); 
		FocusRange range(error);
		double error_sum=0;
		for ( auto f :range ) {
			error_sum+= sqr(error->get(f));
		}
		EXPECT_LE(sqrt(error_sum/range.size()), 1e-4);
	}
	
	// homogeneous diffusion
	model.setParam("D","Dc");
	model.run();
	for (auto err_fun : {"fx_error", "fy_error", "cx_error", "cy_error" }) {
		auto error = SIM::findGlobalSymbol<double>(err_fun); 
		FocusRange range(error);
		double error_sum=0;
		for ( auto f :range ) {
			error_sum+= sqr(error->get(f));
		}
		EXPECT_LE(sqrt(error_sum/range.size()), 1e-4);
	}
}


TEST (FieldDiffusion, Hexagon) {
	
	auto file1 = ImportFile("field_diffusion_002.xml");
	auto model = TestModel(file1.getDataAsString());

	model.run();
	
	auto field = SIM::findGlobalSymbol<double>("f");
	
	auto initial_mass = SIM::findGlobalSymbol<double>("initial_mass") -> get(SymbolFocus::global);
	auto mass = SIM::findGlobalSymbol<double>("mass") -> get(SymbolFocus::global);
	EXPECT_NEAR(mass, initial_mass, initial_mass*mass_error_tolerance_factor);
	
	auto total_error = SIM::findGlobalSymbol<double>("total_error") -> get(SymbolFocus::global);
	EXPECT_NEAR(total_error/initial_mass, 0, operator_error_tolerance_factor);
}

TEST (FieldDiffusion, HexagonDomain) {
	
	auto file1 = ImportFile("field_diffusion_005.xml");
	auto model = TestModel(file1.getDataAsString());

	model.run();
	
	auto field = SIM::findGlobalSymbol<double>("f");
	
	auto initial_mass = SIM::findGlobalSymbol<double>("initial_mass") -> get(SymbolFocus::global);
	auto mass = SIM::findGlobalSymbol<double>("mass") -> get(SymbolFocus::global);
	EXPECT_NEAR(mass, initial_mass, initial_mass*mass_error_tolerance_factor);
	
	auto total_error = SIM::findGlobalSymbol<double>("total_error") -> get(SymbolFocus::global);
	EXPECT_NEAR(total_error/initial_mass, 0, operator_error_tolerance_factor);
}


TEST (FieldBoundary, Hexagon) {
	
	auto file1 = ImportFile("field_flux_boundary_hexagonal.xml");
	auto model = TestModel(file1.getDataAsString());

	// heterogeneous heterogeneous
	model.setParam("D","Df");
	model.run();
	for (auto err_fun : {"fx_error", "fy_error", "cx_error", "cy_error" }) {
		auto error = SIM::findGlobalSymbol<double>(err_fun); 
		FocusRange range(error);
		double error_sum=0;
		for ( auto f :range ) {
			error_sum+= sqr(error->get(f));
		}
		EXPECT_LE(sqrt(error_sum/range.size()), 1e-4);
	}
	
	// homogeneous diffusion
	model.setParam("D","Dc");
	model.run();
	for (auto err_fun : {"fx_error", "fy_error", "cx_error", "cy_error" }) {
		auto error = SIM::findGlobalSymbol<double>(err_fun); 
		FocusRange range(error);
		double error_sum=0;
		for ( auto f :range ) {
			error_sum+= sqr(error->get(f));
		}
		EXPECT_LE(sqrt(error_sum/range.size()), 1e-4);
	}
}

TEST (FieldDiffusion, Cubic) {
	
	auto file1 = ImportFile("field_diffusion_003.xml");
	auto model = TestModel(file1.getDataAsString());
	
	model.run();
	
	auto initial_mass = SIM::findGlobalSymbol<double>("initial_mass") -> get(SymbolFocus::global);
	auto mass = SIM::findGlobalSymbol<double>("mass") -> get(SymbolFocus::global);
	EXPECT_NEAR(mass, initial_mass, initial_mass*mass_error_tolerance_factor);
	
	auto total_error = SIM::findGlobalSymbol<double>("total_error") -> get(SymbolFocus::global);
	EXPECT_NEAR(total_error/initial_mass, 0, operator_error_tolerance_factor);
}

TEST (FieldBoundary, Cubic) {
	
	auto file1 = ImportFile("field_flux_boundary_cubic.xml");
	auto model = TestModel(file1.getDataAsString());

	// heterogeneous heterogeneous
	model.setParam("D","Df");
	model.run();
	for (auto err_fun : {"fx_error", "fy_error", "fz_error", "cx_error", "cy_error", "cz_error" }) {
		auto error = SIM::findGlobalSymbol<double>(err_fun); 
		FocusRange range(error);
		double error_sum=0;
		for ( auto f :range ) {
			error_sum+= sqr(error->get(f));
		}
		EXPECT_LE(sqrt(error_sum/range.size()), 1e-4);
	}
	
	// homogeneous diffusion
	model.setParam("D","Dc");
	model.run();
	for (auto err_fun : {"fx_error", "fy_error", "fz_error", "cx_error", "cy_error", "cz_error" }) {
		auto error = SIM::findGlobalSymbol<double>(err_fun); 
		FocusRange range(error);
		double error_sum=0;
		for ( auto f :range ) {
			error_sum+= sqr(error->get(f));
		}
		EXPECT_LE(sqrt(error_sum/range.size()), 1e-4);
	}
}
