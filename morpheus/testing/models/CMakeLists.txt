set_property(DIRECTORY PROPERTY LABELS "Models")
add_executable(runModelTests M7677_Brook2020/m7677_test.cpp)
InjectModels(runModelTests)
target_link_libraries_patched(runModelTests PRIVATE ModelTesting gtest gtest_main)

gtest_discover_tests(runModelTests WORKING_DIRECTORY "${PROJECT_DIR}")
