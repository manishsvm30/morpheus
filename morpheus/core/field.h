//////
//
// This file is part of the modelling and simulation framework 'Morpheus',
// and is made available under the terms of the BSD 3-clause license (see LICENSE
// file that comes with the distribution or https://opensource.org/licenses/BSD-3-Clause).
//
// Authors:  Joern Starruss and Walter de Back
// Copyright 2009-2022, Technische Universität Dresden, Germany
//
//////


#ifndef PDE_LAYER_H
#define PDE_LAYER_H

#include "lattice_data_layer.h"
#include "config.h"
#include "interfaces.h"
#include <iostream>
#include <fstream>
#include <iterator>

/**
\defgroup ML_Field Field
\ingroup Symbols
\ingroup ML_Global

A \b Field defines a variable scalar field, associating a scalar value to every lattice site, and defines a symbol with it.
Spatio-temporal dynamics can be implemented explicitly by using an \ref ML_Equation, an \ref ML_Event or by using \ref ML_DiffEqn, which also allows
to specify Reaction-(Advection)-Diffusion systems. 

- \b value: \b initial condition for the scalar field. May be given as \ref MathExpressions, also depending on the spatial position (see \ref ML_Space)

Optionally, a \b Diffusion rate may be specified.
- \b rate: diffusion coefficient [(node length)² per (global time)]
- \b well-mixed (optional): if true, homogenizes scalar field. If enabled, diffusion \b rate is ignored.

\b BoundaryValue defines the boundary conditions of the \ref ML_Field at the respective boundary.  By default, the \ref ML_Lattice boundary type definitions are set and the default value/flux tuple is {0,-} for \c constant, and {resp. value within space,0} for \c no-flux and \c flux boundaries.\n \b Note! Boundaries predefined as periodic are immutable.
- \b boundary: a boundary of the model space. (\ref ML_Lattice)
- \b value: mathematical expression providing the value or the flux for the respective boundary type that may vary in space and time. 
- \b type (optional): type of boundary value. If no \b type is specified, the \ref ML_Lattice defined type is preserved.
  - \c value (default): \b value attribute specifies the value at the boundary, leaving the boundary type untouched.
  - \c constant and \c noflux: \b value attribute specifies the value at the boundary and the respective boundary condition, potentially overriding the predefined type.
  - \c flux: \b value attribute specifies the influx at the boundary, i.e. the first order derivative at the boundary, potentially overriding the predefined boundary type\n

\b TiffReader:  specify the initial condition of the field in terms of a float tiff image


**/




/**
\defgroup ML_VectorField VectorField
\ingroup Symbols
\ingroup ML_Global

A \b VectorField  defines a variable vector field, associating an (x,y,z) value to every lattice site, and defines a \b symbol for it.
Spatio-temporal dynamics can be implemented explicitly by using a \ref ML_VectorEquation or \ref ML_VectorRule.
The \ref ML_Space symbol allows the initial expression to directly depend on the spatial position. 

- \b value: \b initial condition for the vector field. May be given as space-dependent \ref MathExpressions.

\b BoundaryValue defines the value of the \ref ML_VectorField at the respective boundary, which may depend on time and space.  By default, boundary values are constant (0,0,0), or periodic, if defined as such under \ref ML_Lattice.

**/




/** \brief Class representing the State of a scalar Field atop the \ref Lattice_Data_Layer
 * 
 * Also takes care of the implementation of Diffusion on all Lattice types (incl. spherical lattices) and the respective
 * boundary handling.
 */

class PDE_Layer : public Lattice_Data_Layer<double>
{
public:
	static const float NO_VALUE;

	PDE_Layer(shared_ptr<const Lattice> l, double p_node_length, bool surface=false);
	~PDE_Layer();

	class ExpressionReader : public ValueReader {
		public:
			ExpressionReader(const Scope* scope) : scope(scope) {};
			void set(string string_val) override { value = make_unique<ExpressionEvaluator<double> >(string_val, scope); value->init(); };
			bool isSpaceConst() const override { return value->flags().space_const; }
			bool isTimeConst() const override { return value->flags().time_const; };
			double get(const VINT& pos) const override { return value->get(SymbolFocus(pos)); }
			shared_ptr<ValueReader> clone() const override { return make_shared<ExpressionReader>(scope); }
		private:
			unique_ptr<ExpressionEvaluator<double> > value;
			const Scope* scope;
	};

	void loadFromXML(const XMLNode xnode, const Scope* scope);
	const string getXMLPath();
	XMLNode saveToXML() const;
	
	bool restoreData(const XMLNode xnode);
	XMLNode storeData(string filename="") const;

	void init(const SymbolFocus& focus = SymbolFocus::global);
	void setInitializer(shared_ptr<ExpressionEvaluator<double>> init) { initializer = init; }

	double getNodeLength() const { return node_length; };
	void updateNodeLength(double nl); /// Update the physical length the lattice discretization. Used in MembraneProperties of CPM cells that can vary in cell size.

	double sum() const;
	double mean() const;
	double variance() const;
	double min_val() const;
	double max_val() const;
//         valarray<value_type>& getBuffer() { return write_buffer; }
// 	uint get_data_index(const VINT& a) const { return Lattice_Data_Layer<double>::get_data_index(a); };

// 	/// Get the gradient in direction @p theta at position @p pos
// 	double getGrad(const VINT& pos, double theta);
//
// 	/// Get the gradient at position @p pos
// 	VDOUBLE getGrad(const VINT& pos);

	shared_ptr<PDE_Layer> clone() {return shared_ptr<PDE_Layer> ( new PDE_Layer(*this) ); };
	/**
		*  Write the layer data to stream @param out in a space/row/row separated ascii format.
		*/
	void write_ascii(ostream& out) const;
	/**
		*  Write the layer data to stream @param out in a gnuplot ascii format.
		*  @param max_value, min_value -- all values are cropped into the given range of values
		*  @param max_resolution specifies the maximal spatial resolution in terms of data points. You may reduce the amount of data to transfer by reducing the spatial resolution.
		*
		*/
	void write_ascii( ostream& out, float min_value, float max_value, int max_resolution = -1 ) const;

	/**
		*  Write the layer data to stream @param out in a gnuplot binary format.
		*  @param max_resolution specifies the maximal spatial resolution in terms of data points.
		*  Use this to reduce the amount of data to transfer by reducing the spatial resolution.
		*/
	void write_binary(ostream& out, int max_resolution = -1);

private:
	string initial_expression;
	shared_ptr<ExpressionEvaluator<double>> initializer;
	bool initialized;
	const Scope* local_scope;
	bool init_by_restore;
	bool store_data;

	vector<shared_ptr<Plugin> > plugins;
	value_tensor write_buffer_ad;
	
	// some information for Surface diffusion
	xt::xtensor<double,1> theta_y;          // theta of y
	xt::xtensor<uint,1> phi_coarsening;     // Lattice coarsening in phi

	double node_length;
	bool is_surface;

	friend class System;
	friend class FwdEulerADSolver;
};


/// Generic interface classes for container providing Fields as a data store
template <class T>
class FieldAccessor : public SymbolRWAccessorBase<T> {
	public:
		using SymbolRWAccessorBase<T>::SymbolRWAccessorBase;
		/// Context sensitive accessor to the underlying data store
		virtual Lattice_Data_Layer<T>* getField(const SymbolFocus& focus = SymbolFocus::global) const =0;
};

template <>
class FieldAccessor<double> : public SymbolRWAccessorBase<double> {
	public:
		using SymbolRWAccessorBase<double>::SymbolRWAccessorBase;
		/// Context sensitive accessor to the underlying data store
		virtual PDE_Layer* getField(const SymbolFocus& focus = SymbolFocus::global) const =0;
};

class Field : public Plugin {
public:
	DECLARE_PLUGIN("Field");
	Field(): Plugin() { symbol_name.setXMLPath("symbol"); registerPluginParameter(symbol_name); initializing=false; initialized = false;}
	void loadFromXML(const XMLNode node, Scope * scope) override;
	XMLNode saveToXML() const override;
	void init(const Scope * scope) override;
	
	class Symbol : public FieldAccessor<double> {
	public:
		Symbol(Field* parent, string name, string descr): 
			FieldAccessor<double>(name), descr(descr),
			parent(parent)
		{
			this->flags().granularity = Granularity::Node;
		};
		const string&  description() const override { return descr; }
		const string XMLPath() const override { return field->getXMLPath(); } 
		std::string linkType() const override { return "FieldLink"; }
		
		TypeInfo<double>::SReturn get(const SymbolFocus & f) const override { return field->get(f.pos()); }
		TypeInfo<double>::SReturn unsafe_get(const SymbolFocus & f) const override { return field->unsafe_get(f.pos()); }
		
		void init() override {
			if (!field)
				parent->init(SIM::getGlobalScope());
		}
		
		PDE_Layer* getField(const SymbolFocus& focus = SymbolFocus::global) const override { 
			if (!field) const_cast<Symbol*>(this)->init();
			return field.get();
		};

		void set(const SymbolFocus & f, typename TypeInfo<double>::Parameter value) const override { field->set(f.pos(), value); };
		void setBuffer(const SymbolFocus & f, TypeInfo<double>::Parameter value) const override { field->unsafe_setBuffer(f.pos(), value); }
		void applyBuffer() const override { field->swapBuffer(); };
		void applyBuffer(const SymbolFocus & f) const override { field->applyBuffer(f.pos()); }
		
	private: 
		string descr;
		shared_ptr<PDE_Layer> field;
		Field* parent;
		friend Field;
	};
private:
	shared_ptr<Symbol> accessor;
	bool initializing, initialized;
	PluginParameter2<string, XMLValueReader, RequiredPolicy> symbol_name;
};


class VectorField_Layer : public Lattice_Data_Layer<VDOUBLE> {
public: 
	VectorField_Layer(shared_ptr< const Lattice > lattice, double node_length);
	void loadFromXML(XMLNode node, const Scope* scope);
	XMLNode saveToXML() const;
	
	bool restoreData(const XMLNode xnode);
	XMLNode storeData(string filename="") const;
	
	void init(const Scope* scope);

private:
	
	class ExpressionReader : public ValueReader {
		public:
			ExpressionReader(const Scope* scope) : scope(scope) {};
			void set(string string_val) override { value = make_unique<ExpressionEvaluator<VDOUBLE> >(string_val,scope); value->init(); };
			bool isSpaceConst() const override { return value->flags().space_const; }
			bool isTimeConst() const override { return value->flags().time_const; }
			VDOUBLE get(const VINT& pos) const override { return value->get(SymbolFocus(pos)); }
			shared_ptr<ValueReader> clone() const override { return make_shared<ExpressionReader>(scope); }
		private:
			unique_ptr<ExpressionEvaluator<VDOUBLE> > value;
			const Scope* scope;
	};
	
	double node_length;
	string initial_expression;
	bool init_by_restore;
};

class VectorField : public Plugin {
public:
	DECLARE_PLUGIN("VectorField");
	VectorField();
	void loadFromXML(const XMLNode node, Scope * scope) override;
	XMLNode saveToXML() const override;
	void init(const Scope * scope) override;
	
	class Symbol : public SymbolRWAccessorBase<VDOUBLE> {
	public:
		Symbol(VectorField* parent, string name, string descr): SymbolRWAccessorBase<VDOUBLE>(name), descr(descr), parent(parent)
		{
			this->flags().granularity = Granularity::Node;
		};
		const string&  description() const override { return descr; }
		std::string linkType() const override { return "VectorFieldLink"; }
		
		TypeInfo<VDOUBLE>::SReturn get(const SymbolFocus & f) const override { return field->get(f.pos()); }
		void init() override {  
			if (! field) parent->init( SIM::getGlobalScope() );
		};
		shared_ptr<VectorField_Layer> getField() const { return field; };
		void set(const SymbolFocus & f, typename TypeInfo<VDOUBLE>::Parameter value) const override { field->set(f.pos(), value); }
		void setBuffer(const SymbolFocus & f, TypeInfo<VDOUBLE>::Parameter value) const override { field->setBuffer(f.pos(), value); }
		void applyBuffer() const override { field->swapBuffer(); };
		void applyBuffer(const SymbolFocus & f) const override { field->applyBuffer(f.pos()); }
		
	private: 
		string descr;
		shared_ptr<VectorField_Layer> field;
		VectorField* parent;
		friend VectorField;
	};
	
private: 
	shared_ptr<Symbol> accessor;
	bool initializing, initialized;
	PluginParameter2<string, XMLValueReader, RequiredPolicy> symbol_name;
};

#endif
