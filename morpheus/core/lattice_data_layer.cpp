#include "lattice_data_layer.h"
#ifndef LATTICE_DATA_LAYER_CPP
#define LATTICE_DATA_LAYER_CPP

#include "xtensor/xindex_view.hpp"
#include "xtensor/xio.hpp"

// template <class T>
// uint Lattice_Data_Layer<T>::get_shadow_index(const VINT& a) const  { return a*shadow_offset; }

template <class T>
Lattice_Data_Layer<T>::Lattice_Data_Layer(shared_ptr<const Lattice> lattice,int shadow_width, T def_val, string layer_name) :
 shadow_base_width(shadow_width), _lattice(lattice), boundary_types(lattice->get_boundary_types())

{
// 	data = NULL;
	name = layer_name;
	dimensions = lattice->getDimensions();
	structure = lattice->getStructure();

	default_value = def_val;
	default_boundary_value = def_val;
	boundary_values.resize(Boundary::nCodes);
	boundary_flux_values.resize(Boundary::nCodes);
	shared_ptr<ValueReader> default_value_reader =  make_shared<DefaultValueReader>(default_boundary_value);
	shared_ptr<ValueReader> mapping_boundary = make_shared<MappedBoundaryValue>(this);
	for ( int i=0; i<Boundary::nCodes; i++) 
		boundary_values[i] = ((boundary_types[i] == Boundary::flux || boundary_types[i] == Boundary::noflux) && TypeInfo<T>::is_float() ) ? mapping_boundary : default_value_reader;
	for (auto & b_val : boundary_flux_values) b_val = default_value_reader;
	
	// assume that the pde layer covers the whole lattice
	using_buffer = false;
	using_domain = lattice->getDomain().domainType() != Domain::none;
	has_reduction = false;
	l_size = lattice->size();
	
	this->shadow_width = VINT(shadow_base_width,dimensions>1 ? shadow_base_width:0, dimensions>2 ? shadow_base_width:0);
	allocate();
	if (using_domain) {
		boundary_types[Boundary::domain] = lattice->getDomain().boundaryType();
		setDomain();
	}

	reset_boundaries();
}

template <class T> void  Lattice_Data_Layer<T>::allocate() {
	// precalculate some variables
	// TODO shadow width should be reset when using reduxtion !!
	shadow_size = l_size + 2 * shadow_width;
	shadow_size_size_xyz = shadow_size.x * shadow_size.y * shadow_size.z;
	shadow_offset =  VINT(1,shadow_size.x, shadow_size.x*shadow_size.y);
	
	s_origin = VINT(0,0,0); /// origin of the shadow region 
	origin = shadow_width;          /// origin of the lattice
	top = origin + l_size;             /// top of the lattice
	s_top = top + shadow_width;           /// top of the shadow region
	
	// create a data lattice with a 2 node wide halo around
	// such that index = (pos + shadow_shift) * shadow_offset;
	
	// Remove particular dimension from access (does not account for the allocation yet)
	if (has_reduction) {
		if (reduction == Boundary::mz || reduction == Boundary::pz) shadow_offset.z=0;
		if (reduction == Boundary::my || reduction == Boundary::py) shadow_offset.y=0;
		if (reduction == Boundary::mx || reduction == Boundary::px) shadow_offset.x=0;
	}
	data.resize({uint(shadow_size.z), uint(shadow_size.y), uint(shadow_size.x)});
	data.fill( default_value );

	if (using_buffer) {
		cout << "!!! creating write buffer" << endl;;
		write_buffer = xt::full_like(data, default_value);
	}
	
	if (using_domain) {
		cout << "!!! creating domain buffer" << endl;;
// 		domain = xt::full_like(data, Boundary::none); // for some reason this does not work;
		domain.resize(data.shape());
		domain.fill(Boundary::none);
	}
};

template <class T>
void Lattice_Data_Layer<T>::loadFromXML(const XMLNode xNode, shared_ptr<ValueReader> converter ) {
	// read the default value
	getXMLAttribute(xNode,"name",name);
// 	getXMLAttribute(xNode, "DefaultValue/text", default_value);

	string restr_name;
	
	if ( (xNode.nChildNode("Reduction") && getXMLAttribute(xNode,"Reduction/text", restr_name)) || (getXMLAttribute(xNode,"Reduction/value", restr_name)) ) {
		reduction = Boundary::name_to_code(restr_name);
		has_reduction = true;
		allocate();
	}
	
	for (int i=0; i< xNode.nChildNode("BoundaryValue"); i++) {
		
		string code_name;
		XMLNode xbv = xNode.getChildNode("BoundaryValue",i);
		if ( ! getXMLAttribute(xbv,"boundary",code_name) )
			throw(string("Missing boundary definition in BoundaryValue"));
		lower_case(code_name);
		auto code = Boundary::name_to_code(code_name);
		
		string type_name;
		auto type = Boundary::value;
		if (getXMLAttribute(xbv,"type",type_name)) {
			type = Boundary::name_to_type(type_name);
			if (type == Boundary::periodic)
				throw string("BoundaryValue: \"periodic\" is illegal as boundary type override!");
			if (boundary_types[code] == Boundary::periodic)
				throw string("Overriding a globally defined 'periodic' boundary condition is not supported!");
			// don't override any type specification for "value" type
			if (code_name != "value") {
				boundary_types[code] = type;
			}
		}
		
		string str_val;
		if ( ! getXMLAttribute(xbv, "value", str_val) )
			throw(string("Missing or invalid value in BoundaryValue"));
		
		shared_ptr<ValueReader> val = converter->clone();
		val->set(str_val);
		
		if (boundary_types[code] == Boundary::value)
			boundary_values[code] = val;
		else if (boundary_types[code] == Boundary::flux) {
			boundary_flux_values[code] =  val;
			if (dynamic_pointer_cast<DefaultValueReader>(boundary_values[code]))
				boundary_values[code] = make_shared<MappedBoundaryValue>(this);
		}
		else if (boundary_types[code] == Boundary::noflux)
			boundary_values[code] = val;
		else 
			throw string("Setting boundary value for boundary code '") + Boundary::type_to_name( boundary_types[code] ) + "' not allowed!";
	}
	
	if (using_domain)
		setDomain();
	reset_boundaries();
	
	stored_node = xNode;
}

//save lattice to XML-file
template <class T> void Lattice_Data_Layer<T>::setDomain()
{
// 	cout << "Creating domain map" << endl;
	using_domain = true;
	if ( domain.size() != data.size())
		domain.resize(data.shape(),Boundary::none);

	VINT pos;
	const Domain& d = _lattice->getDomain();
	for (pos.z=0; pos.z<l_size.z; pos.z++) {
		for (pos.y=0; pos.y<l_size.y; pos.y++) {
			for (pos.x=0; pos.x<l_size.x; pos.x++) {
				auto dpos = pos + origin;
				
				if (d.inside(pos)) {
					domain(dpos.z,dpos.y,dpos.x) = Boundary::none;
					data(dpos.z,dpos.y,dpos.x) = default_value;
				}
				else {
					domain(dpos.z,dpos.y,dpos.x) = boundary_types[Boundary::domain];
					data(dpos.z,dpos.y,dpos.x) = boundary_values[Boundary::domain]->get(pos);
				}
			}
		}
	}
	
	xt::view(domain, xt::range(s_origin.z,origin.z), xt::all(), xt::all()) = boundary_types[Boundary::mz];
	xt::view(domain, xt::range(top.z,s_top.z),       xt::all(), xt::all()) = boundary_types[Boundary::pz];
	xt::view(domain, xt::all(), xt::range(s_origin.y,origin.y), xt::all()) = boundary_types[Boundary::my];
	xt::view(domain, xt::all(), xt::range(top.y,s_top.y),       xt::all()) = boundary_types[Boundary::py];
	xt::view(domain, xt::all(), xt::all(), xt::range(s_origin.x,origin.x)) = boundary_types[Boundary::mx];
	xt::view(domain, xt::all(), xt::all(), xt::range(top.x,s_top.x)      ) = boundary_types[Boundary::px];
	
	VINT i_origin = origin + shadow_width;
	VINT i_top = top - shadow_width;
	
	if (boundary_types[Boundary::mx] == Boundary::periodic) {
		xt::view(domain, xt::all(), xt::all(), xt::range(s_origin.x,origin.x))
			= xt::view(domain, xt::all(), xt::all(), xt::range(i_top.x, top.x)     );
		xt::view(domain, xt::all(), xt::all(), xt::range(top.x,s_top.x)      )
			= xt::view(domain, xt::all(), xt::all(), xt::range(origin.x,i_origin.x));
	}
	if (boundary_types[Boundary::my] == Boundary::periodic) {
		xt::view(domain, xt::all(), xt::range(s_origin.y,origin.y), xt::all())
			= xt::view(domain, xt::all(), xt::range(i_top.y, top.y),      xt::all());
		xt::view(domain, xt::all(), xt::range(top.y,s_top.y),       xt::all())
			= xt::view(domain, xt::all(), xt::range(origin.y,i_origin.y), xt::all());
	}
	if (boundary_types[Boundary::mz] == Boundary::periodic) {
		xt::view(domain, xt::range(s_origin.z,origin.z), xt::all(), xt::all())
			= xt::view(domain, xt::range(i_top.z, top.z),      xt::all(), xt::all());
		xt::view(domain, xt::range(top.z,s_top.z),       xt::all(), xt::all())
			= xt::view(domain, xt::range(origin.z,i_origin.z), xt::all(), xt::all());
	}
}

//save lattice to XML-file
template <class T> XMLNode Lattice_Data_Layer<T>::saveToXML() const
{
	return stored_node;
}

template <class T>
void Lattice_Data_Layer<T>::storeData(ostream &out) const {
	const bool binary = false;
	
	if ( ! binary ) {
		string xsep(" "), ysep("\n"), zsep("\n");
	
		VINT pos;
		for (pos.z=0; pos.z<l_size.z; pos.z++) {
			for (pos.y=0; pos.y<l_size.y; pos.y++) {
				pos.x=0;
				out << this->get(pos);
				for (pos.x=1; pos.x<l_size.x; pos.x++) {
					out << xsep << this->get(pos);
				}
				if (pos.y+1<l_size.y) out << ysep;
			}
			if (pos.z+1<l_size.z) out << zsep;
		}
	}
}
	
template <class T>
bool Lattice_Data_Layer<T>::restoreData(istream& in,  T (* converter)(istream &)) {
	// iterate over the data points
	VINT pos;
	for (pos.z=origin.z; pos.z<top.z; pos.z++) {
		for (pos.y=origin.y; pos.y<top.y; pos.y++) {
			for (pos.x=origin.x; pos.x<top.x; pos.x++) {
				data(pos.z,pos.y,pos.x) = converter(in);
				if (in.fail()) {
					if (in.eof())
						throw (string("Lattice_Data_Layer<T>::restoreData: Not sufficient data in input stream"));
					else
						throw (string("Lattice_Data_Layer<T>::restoreData: Cannot convert values input stream"));
					return false;
				}
			}
		}
	}
	reset_boundaries();
	return true;
}


template <class T>
Boundary::Type Lattice_Data_Layer<T>::getBoundaryType(Boundary::Codes code) const
{
	return boundary_types[code];
}

template <class T> 
typename TypeInfo<T>::Return Lattice_Data_Layer<T>::get(VINT a) const {
		if ( accessible(a) ) {
			a+=origin;
			return data(a.z,a.y,a.x);
		}
		else if ( _lattice->resolve(a)) {
			a+=origin;
			return data(a.z,a.y,a.x);
		}
		else
			throw string("Unable to access position ") + to_str(a);
// 		return boundary_values[b]->get(a);
// 	}
};

template <class T> 
bool Lattice_Data_Layer<T>::set(VINT a, typename TypeInfo<T>::Parameter b) {
// 	if ( lattice->accessible(a) ) {
// 		data[get_data_index(a)] = b;
// 	}
	VINT periodic_shifts(0,0,0);
	
	if ( this->writable_resolve(a)) {
		unsafe_set(a,b);
		if (boundary_types[Boundary::px] == Boundary::periodic) {
			if (a.x<shadow_width.x) {
				periodic_shifts.x = l_size.x;
				unsafe_set(a+periodic_shifts,b);
			} else if (a.x > l_size.x-shadow_width.x-1) {
				periodic_shifts.x =  -l_size.x;
				unsafe_set(a+periodic_shifts,b);
			}
		}
		if (boundary_types[Boundary::py] == Boundary::periodic) {
			if (a.y<shadow_width.y) {
				periodic_shifts.y = l_size.y;
				auto p = a+periodic_shifts;
				unsafe_set(p,b);
				if (periodic_shifts.x!=0) {
					p.x-=periodic_shifts.x;
					unsafe_set(p,b);
				}
			} else if (a.y > l_size.y-shadow_width.y-1) {
				periodic_shifts.y = -l_size.y;
				auto p = a+periodic_shifts;
				unsafe_set(p,b);
				if (periodic_shifts.x!=0) {
					p.x -= periodic_shifts.x;
					unsafe_set(p,b);
				}
			}
			
		}
		if (boundary_types[Boundary::pz] == Boundary::periodic) {
			if (a.z<shadow_width.z) {
				periodic_shifts.z = l_size.z;
			} else if (a.z > l_size.z-shadow_width.z-1) {
				periodic_shifts.z = -l_size.z;
			}
			if (periodic_shifts.z!=0) {
				auto p = a+periodic_shifts;
				unsafe_set(p,b);
				if (periodic_shifts.y!=0) {
					p.y -= periodic_shifts.y;
					unsafe_set(p,b);
					if (periodic_shifts.x!=0) {
						p.x -= periodic_shifts.x;
						unsafe_set(p,b);
						p.y += periodic_shifts.y;
						unsafe_set(p,b);
					}
				}
				else if (periodic_shifts.x!=0) {
					p.x -= periodic_shifts.x;
					unsafe_set(p,b);
				}
			}
		}
		return true;
	}
	return false;
}

template <class T> 
bool Lattice_Data_Layer<T>::accessible (const VINT& a) const {
	return ( (a.z>=-shadow_width.z && a.z<l_size.z+shadow_width.z) && (a.y>=-shadow_width.y && a.y<l_size.y+shadow_width.y) && (a.x >=-shadow_width.x && a.x<l_size.x + shadow_width.x) );
}

template <class T> bool Lattice_Data_Layer<T>::writable(const VINT& a) const {
	if (using_domain) {
		VINT b(a);
		if ( !_lattice->resolve(b) )
			return false;
		b+=origin;
		return (domain(b.z,b.y,b.x) == Boundary::none);
	}
	else
		return _lattice->inside(a);
}

template <class T> bool Lattice_Data_Layer<T>::writable_resolve(VINT& a) const {
	if (using_domain) {
		if ( ! _lattice->resolve(a) )
			return false;
// 		assert(accessible(a));
		VINT b = a+origin;
		return (domain(b.z,b.y,b.x) == Boundary::none);
	}
	else {
		return _lattice->resolve(a);
		
	}
}

template <class T> bool Lattice_Data_Layer<T>::writable_resolve(VINT& a, Boundary::Type& b) const {

	Boundary::Codes code;
	if ( ! _lattice->resolve(a,code) ) {
		b = boundary_types[code];
		return false;
	}
// 	assert(accessible(a));
	if (using_domain) {
		auto c = a+origin;
		b = domain(c.z,c.y,c.x);
		return b==Boundary::none;
	}
	else  {
		b=Boundary::none;
		return true;
	}
}

// template <class T> T& Lattice_Data_Layer<T>::get_writable(VINT a) {
// 	if ( writable_resolve(a)) {
// // 		assert(accessible(a));
// 		return data[get_data_index(a)];
// 	}
// 	assert(0);
// 	return shit_value;
// }


template <class T> VINT Lattice_Data_Layer<T>::getWritableSize(){
	if (has_reduction) {
		switch (reduction) {
			case Boundary::mz : 
			case Boundary::pz :
				return VINT(l_size.x,l_size.y,1);
			case Boundary::mx : 
			case Boundary::px :
				return VINT(1,l_size.y,l_size.z);
			case Boundary::my : 
			case Boundary::py :
				return VINT(l_size.x,1,l_size.z);
			default:
				return size();
		}
	}
	else {
		return size();
	}
};

template <class T> 
vector<VINT> Lattice_Data_Layer<T>::optimizeNeighborhood(const vector<VINT>& a) const {
	vector<VINT> t(a);
	sort(t.begin(),t.end(), less_memory_position);
	return t;
}

template <class T> 
bool Lattice_Data_Layer<T>::less_memory_position(const VINT& a, const VINT& b) {
	return a.z < b.z || ( a.z == b.z && a.y < b.y ) || ( a.z == b.z && a.y == b.y && a.x < b.x );
	
}

template <class T>
void Lattice_Data_Layer<T>::reset_boundaries() {
	// set no-flux boundaries to the neighboring site values
	VINT pos;
	
	if (using_domain && ! boundary_values[Boundary::domain]->isTimeConst()) {
		if (boundary_values[Boundary::domain]->isSpaceConst()) {
			T val = boundary_values[Boundary::domain]->get(VINT(0,0,0));
			xt::filter(data, xt::not_equal(domain,Boundary::none) ).fill(val);
		}
		else {
			for (pos.z=0; pos.z<l_size.z; pos.z++) {
				for (pos.y=0; pos.y<l_size.y; pos.y++) {
					for (pos.x=0; pos.x<l_size.x; pos.x++) {
						auto p = pos+origin;
						if (domain(p.z,p.y,p.x) != Boundary::none)
							data(p.z,p.y,p.x) = boundary_values[Boundary::domain]->get(pos);
					}
				}
			}
		}
	}
	/// top/origin shifted inside lattice
	VINT i_origin = origin +shadow_width;
	VINT i_top = top - shadow_width;
	auto xmb = xt::view(data, xt::all(), xt::all(), xt::range(s_origin.x,origin.x));
	auto xpb = xt::view(data, xt::all(), xt::all(), xt::range(top.x,s_top.x));
	if (boundary_types[Boundary::mx] == Boundary::periodic) {
		xmb = xt::view(data, xt::all(), xt::all(), xt::range(i_top.x, top.x));
		xpb = xt::view(data, xt::all(), xt::all(), xt::range(origin.x, i_origin.x));
	}
	else {
		if (boundary_values[Boundary::mx]->isSpaceConst()) {
			xmb.fill( boundary_values[Boundary::mx]->get(VINT(0,0,0)) );
		}
		else {
			VINT start = s_origin;
			VINT stop  = s_top;
			stop.x = origin.x;
			pos = start;
			
			for (; pos.z<stop.z; pos.z++)
				for (pos.y=start.y; pos.y<stop.y; pos.y++)
					for (pos.x=start.x; pos.x<stop.x; pos.x++) {
						data(pos.z,pos.y,pos.x) = boundary_values[Boundary::mx]->get(pos-origin);
					}
		}
			
		if (boundary_values[Boundary::px]->isSpaceConst()) {
			xpb.fill( boundary_values[Boundary::px]->get(VINT(0,0,0)) );
		}
		else {
			VINT start = s_origin;
			VINT stop  = s_top;
			start.x = top.x; 
			pos = start;
			
			for (; pos.z<stop.z; pos.z++)
				for (pos.y=start.y; pos.y<stop.y; pos.y++)
					for (pos.x=start.x; pos.x<stop.x; pos.x++) {
						data(pos.z,pos.y,pos.x) = boundary_values[Boundary::px]->get(pos-origin);
					}
		}
	}
	if (dimensions>1) {
		auto ymb = xt::view(data, xt::all(), xt::range(s_origin.y,origin.y), xt::all());
		auto ypb = xt::view(data, xt::all(), xt::range(top.y,s_top.y),       xt::all());
		
		if (boundary_types[Boundary::my] == Boundary::periodic) {
			ymb = xt::view(data, xt::all(), xt::range(i_top.y, top.y),      xt::all());
		    ypb = xt::view(data, xt::all(), xt::range(origin.y,i_origin.y), xt::all());
		}
		else {
			if (boundary_values[Boundary::my]->isSpaceConst()) {
				ymb = boundary_values[Boundary::my]->get(VINT(0,0,0));
			}
			else {
				VINT start = s_origin;
				VINT stop  = s_top;
				stop.y = origin.y;
				pos = start;

				for (; pos.z<stop.z; pos.z++)
					for (pos.y=start.y; pos.y<stop.y; pos.y++)
						for (pos.x=start.x; pos.x<stop.x; pos.x++) {
							data(pos.z,pos.y,pos.x) = boundary_values[Boundary::my]->get(pos-origin);
						}
			}
				
			if (boundary_values[Boundary::py]->isSpaceConst()) {
				ypb = boundary_values[Boundary::py]->get(VINT(0,0,0));
			}
			else {
				VINT start = s_origin;
				VINT stop  = s_top;
				start.y = top.y;
				pos = start;

				for (; pos.z<stop.z; pos.z++)
					for (pos.y=start.y; pos.y<stop.y; pos.y++)
						for (pos.x=start.x; pos.x<stop.x; pos.x++) {
							data(pos.z,pos.y,pos.x) = boundary_values[Boundary::py]->get(pos-origin);
						}
			}
		}
	}
	if (dimensions>2) {
		auto zmb = xt::view(data, xt::range(s_origin.z,origin.z), xt::all(), xt::all());
		auto zpb = xt::view(data, xt::range(top.z,s_top.z),       xt::all(), xt::all());
		
		if (boundary_types[Boundary::mz] == Boundary::periodic) {
			zmb = xt::view(data, xt::range(i_top.z, top.z),      xt::all(), xt::all());
			zpb = xt::view(data, xt::range(origin.z,i_origin.z), xt::all(), xt::all());
		}
		else {
			if (boundary_values[Boundary::mz]->isSpaceConst()) {
				zmb = boundary_values[Boundary::mz]->get(VINT(0,0,0));
			}
			else {
				VINT start = s_origin;
				VINT stop  = s_top;
				stop.z = origin.z;
				pos = start;
				
				for (; pos.z<stop.z; pos.z++)
					for (pos.y=start.y; pos.y<stop.y; pos.y++)
						for (pos.x=start.x; pos.x<stop.x; pos.x++) {
							data(pos.z,pos.y,pos.x) = boundary_values[Boundary::mz]->get(pos-origin);
						}
			}
			
			if (boundary_values[Boundary::pz]->isSpaceConst()) {
				zpb = boundary_values[Boundary::pz]->get(VINT(0,0,0));
			}
			else {
				VINT start = s_origin;
				VINT stop  = s_top;
				start.z = top.z;
				pos = start;
				
				for (; pos.z<stop.z; pos.z++)
					for (pos.y=start.y; pos.y<stop.y; pos.y++)
						for (pos.x=start.x; pos.x<stop.x; pos.x++) {
							data(pos.z,pos.y,pos.x) = boundary_values[Boundary::pz]->get(pos-origin);
						}
			}
		}
	}
}

template <class T>
void Lattice_Data_Layer<T>::useBuffer(bool b) {
	if (b) {
		using_buffer = true;
		write_buffer.resize(data.shape());
		write_buffer.fill( default_value );
	}
	else {
		using_buffer = false;
		write_buffer = {{{ default_value }}};
	}
}

template <class T>
bool Lattice_Data_Layer<T>::setBuffer(const VINT& pos, typename TypeInfo<T>::Parameter value)
{
	assert(using_buffer);
	if (!writable(pos) ) return false;
	auto p = pos+origin;
	write_buffer(p.z,p.y,p.x) = value;
	return true;
}

template <class T>
typename TypeInfo<T>::Return Lattice_Data_Layer<T>::getBuffer(const VINT& pos) const {
	assert(using_buffer); auto p = pos+origin; return write_buffer(p.x,p.y,p.z);
}

template <class T>
void Lattice_Data_Layer<T>::applyBuffer(const VINT& pos) {
	 auto p = pos+origin;
	 data(p.z,p.y,p.x) = write_buffer(p.z,p.y,p.x);
}

template <class T>
void Lattice_Data_Layer<T>::copyDataToBuffer() {
	write_buffer = data;
}

template <class T>
void Lattice_Data_Layer<T>::swapBuffer()
{
	assert(using_buffer);
	std::swap(data, write_buffer);
	reset_boundaries();
}



#endif // LATTICE_DATA_LAYER_CPP
