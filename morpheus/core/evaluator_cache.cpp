#include "evaluator_cache.h"
#include <string_view>

EvaluatorCache::NS::NS(string name, const Scope* scope, vector<double>& value_cache) :
 scope(scope), ns_name(name), value_cache(value_cache) {}

set<Symbol> EvaluatorCache::NS::getUsedSymbols() const
{
	set<Symbol> dep;
	for (const auto& sym : used_symbols) {
		dep.insert(sym.sym);
	}
	return dep;
}

void EvaluatorCache::NS::setFocus(const SymbolFocus& f) const
{
	for (const auto& sd : used_symbols) {
// 		auto& sd = sym.second;
		if (sd.type == SymbolDesc::DOUBLE) {
			auto accessor = static_cast<const SymbolAccessorBase<double>*>(sd.sym.get());
			value_cache[sd.cache_idx] = accessor->get(f);
		}
		else {
			auto accessor = static_cast<const SymbolAccessorBase<VDOUBLE>*>(sd.sym.get());
			VDOUBLE value = accessor->get(f);
			value_cache[sd.cache_idx] = value.x;
			value_cache[sd.cache_idx+1] = value.y;
			value_cache[sd.cache_idx+2] = value.z;
		}
	}
}

uint EvaluatorCache::addNameSpaceScope(const string& ns_name, const Scope* scope)
{
	uint ns_id = external_namespaces.size();
	external_namespaces.push_back(NS(ns_name,scope, value_cache));
	return ns_id;
}

set<Symbol> EvaluatorCache::getNameSpaceUsedSymbols(uint ns_id) const {
	if (ns_id>external_namespaces.size())
		throw string("EvaluatorCache: Invalid namespace id");
	return external_namespaces[ns_id].getUsedSymbols();
}

void EvaluatorCache::setNameSpaceFocus(uint ns_id, const SymbolFocus& f) const {
	if (ns_id>external_namespaces.size())
		throw string("EvaluatorCache: Invalid namespace id");
	external_namespaces[ns_id].setFocus(f);
}


EvaluatorCache::EvaluatorCache(const EvaluatorCache& other)
{
	local_scope = other.local_scope ;
	allow_partial_spec = other.allow_partial_spec;
	no_val = other.no_val;

	cache_symbols = other.cache_symbols;
	value_cache = other.value_cache;
	finalized = false;
	
// 	// Local variable storage
	locals_table = other.locals_table;
	locals_cache = other.locals_cache;
	parser_symbols = other.parser_symbols;
	
// 	// External variable storage
// 	flat_externals = other.flat_externals;
	for (const auto & ns : other.external_namespaces){ 
		external_namespaces.push_back(NS(ns,value_cache));
	}
	
	// infrastructure for vector symbol expansion
	scalar_expansion_permitted  = other.scalar_expansion_permitted;
	scalar_expansion = other.scalar_expansion;
	expansions = other.expansions;

}


mu::value_type* EvaluatorCache::registerSymbol(const mu::char_type* symbol, void* ptr) {
	auto that = reinterpret_cast<EvaluatorCache*>(ptr);
	return that->registerSymbol_internal(symbol);
}

mu::value_type* EvaluatorCache::registerSymbol_internal(const mu::char_type* sym) {
// 	if (locals_callback) {
// 		// Try to dynamically add the requested symbol to the locals
// 		locals_callback(symbol,this);
// 	}
	string symbol = sym;
	
	auto it_sym = cache_symbols.find(symbol);
	if (it_sym!=cache_symbols.end()) {
		const auto& symbol = it_sym->second;
		if (symbol.type == SymbolDesc::DOUBLE) {
			if (symbol.source == SymbolDesc::LOCAL) {
				return &locals_cache[symbol.cache_idx];
			}
			else {
				return &no_val;
			}
		}
		else if (scalar_expansion_permitted && it_sym->second.type == SymbolDesc::VECTOR) {
			auto exp_it = expansions.find(symbol.name);
			if (exp_it == expansions.end()) {
				/// create the scalar expansion wrapper
				ExpansionDesc exp;
				exp.source = symbol.source;
				exp.cache_source_idx = symbol.cache_idx;
				exp.cache_dest_idx = value_cache.size();
				value_cache.push_back(0);
				exp_it = expansions.insert( {symbol.name, exp} ).first;
				scalar_expansion = true;
			}
			return &no_val;
		}
	}
	
	// Try to register the symbol dynamically
	
	const Scope* search_scope = local_scope;
	string search_symbol = symbol;
	NS* ns = nullptr;
	// extract leading namespace
	if (symbol.find('.') != string::npos) {
		string ns_name = symbol.substr(0,symbol.find('.'));
		string ns_symbol = symbol.substr(symbol.find('.')+1, string::npos);
		// Try to find a suitible name space
		for ( auto& l_ns : external_namespaces ) {
			if ( l_ns.ns_name == ns_name ) {
				ns = &l_ns;
				search_scope = l_ns.scope;
				search_symbol = ns_symbol;
			}
		}
	}
	// From search scope
	if (search_scope->getAllSymbolNames<double>(true).count(search_symbol)) {
		SymbolDesc sd;
		sd.name = search_symbol;
		sd.sym  = search_scope->findSymbol<double>(search_symbol, true);
		if (sd.sym->flags().partially_defined && ! allow_partial_spec) {
			mu::Parser::exception_type e(mu::ecINVALID_NAME, string("Symbol '") + symbol + "' is only partially defined, i.e. not in all spatial scopes (CellTypes)!");
			cout << "EvaluatorCache::registerSymbol(): Symbol " << symbol << " is not defined in all spatial scopes (CellTypes) of scope " << search_scope->getName() << "!" << endl;
			throw e;
		}
		sd.type = SymbolDesc::DOUBLE;
		sd.source = ns ? SymbolDesc::NS : SymbolDesc::EXTERNAL;
		sd.cache_idx = value_cache.size();
		value_cache.push_back(0);
		
		cache_symbols.insert({ symbol, sd });
		flat_externals.push_back(sd);
		if (ns) ns->used_symbols.push_back(sd);
		return &no_val;
	}
	if (scalar_expansion_permitted && search_scope->getAllSymbolNames<VDOUBLE>(true).count(search_symbol)) {
		/// create the scalar expansion wrapper

		SymbolDesc sd;
		sd.name = search_symbol;
		sd.sym = search_scope->findSymbol<VDOUBLE>(search_symbol, true);
		if (sd.sym->flags().partially_defined && ! allow_partial_spec) {
			mu::Parser::exception_type e(mu::ecINVALID_NAME, string("Symbol '") + symbol + "' is only partially defined, i.e. not in all spatial scopes (CellTypes)!");
			cout << "EvaluatorCache::registerSymbol(): Symbol " << symbol << " is not defined in all spatial scopes (CellTypes) of scope " << search_scope->getName() << "!" << endl;
			throw e;
		}
		sd.type = SymbolDesc::VECTOR;
		sd.source = ns ? SymbolDesc::NS : SymbolDesc::EXTERNAL;
		sd.cache_idx = value_cache.size();
		value_cache.push_back(0);value_cache.push_back(0);value_cache.push_back(0);
		cache_symbols.insert({symbol,sd});
		flat_externals.push_back(sd);
		if (ns) ns->used_symbols.push_back(sd);
	
		ExpansionDesc exp;
		exp.source = sd.source;
		exp.cache_source_idx = sd.cache_idx;
		exp.cache_dest_idx = value_cache.size(); value_cache.push_back(0);
		expansions.insert( {symbol, exp});
		scalar_expansion = true;
		
		return &no_val;
	}
	else {
		mu::Parser::exception_type e(mu::ecINVALID_NAME,symbol);
		cout << "EvaluatorCache::registerSymbol(): Unknown symbol " << symbol << endl;
		cout << "Available is ";
		if (ns) cout << " in namespace \"" << ns->ns_name << "\" ";
		set<string > all_symbols = search_scope->getAllSymbolNames<double>(allow_partial_spec);
		copy(all_symbols.begin(), all_symbols.end(),ostream_iterator<string>(cout,","));
		for (const auto& sym : cache_symbols) {
			if (sym.second.source == SymbolDesc::LOCAL)
				cout << ", " << sym.first;
		}
		cout << endl;
		throw e;
	}
	return  &no_val;
}

/// Add a local symbol @p name to the cache and return it's cache position
int EvaluatorCache::addLocal(string name, double value) {
	if (cache_symbols.count(name)) throw string("Cannot add local in EvaluatorCache! Symbol ") + name + " already exists";
	SymbolDesc sd;
	sd.name = name;
	sd.source = SymbolDesc::LOCAL;
	sd.type = SymbolDesc::DOUBLE;
	sd.cache_idx = locals_cache.size(); locals_cache.push_back(value);
	cache_symbols.insert({name, sd});
	locals_table.push_back( { name, LocalSymbolDesc::DOUBLE } );
	return sd.cache_idx;
}

/// Add a local symbol @p name to the cache and return it's cache position
int EvaluatorCache::addLocal(string name, VDOUBLE value) {
	if (cache_symbols.count(name)) throw string("Cannot add local in EvaluatorCache! Symbol ") + name + " already exists";
	SymbolDesc sd;
	sd.name = name;
	sd.source = SymbolDesc::LOCAL;
	sd.type = SymbolDesc::VECTOR;
	sd.cache_idx = locals_cache.size();
	locals_cache.push_back(value.x);
	locals_cache.push_back(value.y);
	locals_cache.push_back(value.z);
	cache_symbols.insert({name, sd});
	locals_table.push_back( { sd.name, LocalSymbolDesc::VECTOR } );
	int idx = sd.cache_idx;
	sd.type = SymbolDesc::DOUBLE;
	sd.name = name+".x";
	cache_symbols.insert({sd.name, sd});
	sd.cache_idx++;
	sd.name = name+".y";
	cache_symbols.insert({sd.name, sd});
	sd.cache_idx++;
	sd.name = name+".z";
	cache_symbols.insert({sd.name, sd});	
	return idx;
}

int EvaluatorCache::getLocalIdx(const string& name) {
	auto it_local = cache_symbols.find(name);
	if (it_local == cache_symbols.end())
		throw string("EvaluatorCache::getLocalIdx: Symbol '") + name + "' is not registered in cache";
	if (it_local->second.source != SymbolDesc::LOCAL)
		throw string("EvaluatorCache::getLocalIdx: Symbol '") + name + "' is not a local variable to the cache";
	return it_local->second.cache_idx;
}

void EvaluatorCache::setLocalsTable(const vector<EvaluatorCache::LocalSymbolDesc>& layout)
{

	if ( ! locals_table.empty() )
		throw string("EvaluatorCache::setLocalsTable()  Refuse to override locals. Locals already defined.");
	for (const auto& loc : layout) {
		if (loc.type == LocalSymbolDesc::VECTOR) {
			addLocal(loc.symbol, VDOUBLE(0,0,0));
		}
		else 
			addLocal(loc.symbol, 0);
	}
}

double EvaluatorCache::get(const string& name) {
	auto it_sym = cache_symbols.find(name);
	if (it_sym == cache_symbols.end()) return 0;
	if (it_sym->second.type == SymbolDesc::DOUBLE) {
		if (it_sym->second.source == SymbolDesc::LOCAL) {
			return locals_cache[it_sym->second.cache_idx];
		}
		else {
			return value_cache[it_sym->second.cache_idx];
		}
	}
	else {
		if (expansions.count(name)) {
			return value_cache[expansions[name].cache_dest_idx];
		}
		return 0;
	}
}


EvaluatorCache::ParserDesc EvaluatorCache::getDescriptor(mu::Parser *parser) {
	auto symbols = parser->GetUsedVar();
	ParserDesc desc;
	desc.requires_expansion = false;
	for (const auto& sym : symbols) {
		
		auto it_sym = cache_symbols.find(sym.first);
		if (it_sym != cache_symbols.end())  {
			const auto& symbol = it_sym->second;
			if (symbol.type == SymbolDesc::VECTOR) {
				desc.requires_expansion = true;
			}
			if (symbol.source == SymbolDesc::LOCAL) {
				desc.loc_symbols.insert(symbol.name);
			}
			else {
				desc.ext_symbols.insert(symbol.sym);
			}
		}
		else if (parser_symbols.count(sym.first)) {
			desc.loc_symbols.insert(sym.first);
		}
		else 
			throw string("EvaluatorCache::getDescriptor:  Symbol ") + sym.first + " not registered.";
	}
	return desc;
}

void EvaluatorCache::attach(mu::Parser *parser) {
	attached_parsers.push_back(parser);
	mu::facfun_type factory = EvaluatorCache::registerSymbol;
	parser->SetVarFactory(factory, (void*) this );
	finalized = false;
}

void EvaluatorCache::detach(mu::Parser *parser) {
	auto p = find(attached_parsers.begin(),attached_parsers.end(),parser);
	if (p!=attached_parsers.end()) attached_parsers.erase(p);
// 	for ( auto p = attached_parsers.begin(); p!=attached_parsers.end(); p++) {
// 		if (*p == parser) {
// 			attached_parsers.erase(p);
// 			break;
// 		}
// 	}
}

/// Fill the cache with data wrt. @p focus
void EvaluatorCache::fetch(const SymbolFocus& focus, const bool safe) {
// 	if (current_focus == focus && current_time = SIM::getTime()) return;
	if (!finalized) finalize();
	const string * symbol;
	try {
		for (const auto& sym : flat_externals) {
// 			if (sym.source == SymbolDesc::EXTERNAL) {
				symbol = &sym.name;
				if (sym.type == SymbolDesc::DOUBLE) {
					auto accessor = static_cast<const SymbolAccessorBase<double>* >(sym.sym.get());
					value_cache[sym.cache_idx] = /*safe ? accessor->safe_get(focus) :*/ accessor->get(focus);
				}
				else if (sym.type == SymbolDesc::VECTOR) {
					auto accessor = static_cast<const SymbolAccessorBase<VDOUBLE>* >(sym.sym.get());
					VDOUBLE value = /*safe ? accessor->safe_get(focus) :*/ accessor->get(focus);
					value_cache[sym.cache_idx] = value[0];
					value_cache[sym.cache_idx+1] = value[1];
					value_cache[sym.cache_idx+2] = value[2];
				}
// 			}
		}
	}
	catch (const string& e) {
		throw string("Unable to fetch symbol '") + *symbol + "'.\n" + e;
	}
	// 	current_focus = focus;
}

void EvaluatorCache::finalize() {
	// fetch all constants
	flat_externals.clear();
	// identify external constant symbols and prefetch these
	for (auto& sym : cache_symbols) {
		if (sym.second.type == SymbolDesc::DOUBLE) {
			if (sym.second.source==SymbolDesc::EXTERNAL) {
				auto symbol = static_pointer_cast<const SymbolAccessorBase<double>>(sym.second.sym);
				if (symbol->flags().space_const && symbol->flags().time_const) {
					value_cache[sym.second.cache_idx] = symbol->get(SymbolFocus::global);
					sym.second.source=SymbolDesc::CONSTANT;
				}
				else {
					flat_externals.push_back(sym.second);
				}
			}
		}
		else if (sym.second.type == SymbolDesc::VECTOR) {
			if (sym.second.source == SymbolDesc::EXTERNAL) {
				flat_externals.push_back(sym.second);
			}
		}
	}
	
	std::map<mu::Parser*, mu::varmap_type > used_vars;
	for (auto parser : attached_parsers) {
		auto vars = parser->GetUsedVar();
		used_vars[parser] = vars;
	}
	// Register constants and variables as required to the parsers
	for (auto parser : attached_parsers) {
		auto vars = used_vars[parser];
		for (const auto& var : vars) {
			auto it_sym = cache_symbols.find(var.first);
			if (it_sym==cache_symbols.end())
				throw string("Inconsistency in EvaluatorCache: Unable to find symbol ") +  var.first;
			if (it_sym->second.type == SymbolDesc::DOUBLE) {
				switch (it_sym->second.source) {
					case SymbolDesc::CONSTANT :
						parser->DefineConst(var.first, value_cache[it_sym->second.cache_idx]);
// 						parser->DefineVar(var.first, &value_cache[it_sym->second.cache_idx]);
						break;
					case SymbolDesc::LOCAL :
						parser->DefineVar(var.first, &locals_cache[it_sym->second.cache_idx]);
						break;
					case SymbolDesc::EXTERNAL :
					case SymbolDesc::NS :
						parser->DefineVar(var.first, &value_cache[it_sym->second.cache_idx]);
						break;
				}
			}
			else {
				auto expand = expansions.find(var.first);
				if (expand==expansions.end())
					throw string("Inconsistency in EvaluatorCache: Unable to find expansion for ") + var.first;
				parser->DefineVar(var.first, &value_cache[expand->second.cache_dest_idx]);
			}
		}
	}
	finalized = true;
}

/// A list of used external symbols.
std::set<Symbol> EvaluatorCache::getExternalSymbols() {
	std::set<Symbol> symbols;
	for ( const auto& sym : cache_symbols) {
		if (sym.second.source == SymbolDesc::EXTERNAL)
			symbols.insert(symbols.end(), sym.second.sym);
	}
	return symbols;
}

void EvaluatorCache::setExpansionIndex(uint idx) noexcept {
	if (! scalar_expansion || idx>2) return;
	for (auto& exp : expansions) {
		value_cache[exp.second.cache_dest_idx] = 
			(exp.second.source == SymbolDesc::LOCAL) ? locals_cache[exp.second.cache_source_idx+idx] : value_cache[exp.second.cache_source_idx+idx];
	}
}

string EvaluatorCache::toString() {
	stringstream s;
	for ( const auto& sym : cache_symbols) {
		const auto& cache = sym.second.source == SymbolDesc::LOCAL ? locals_cache : value_cache;
		if (sym.second.type == SymbolDesc::DOUBLE)
			s << sym.first << "=" << cache[sym.second.cache_idx] << "; ";
		else
			s << sym.first << "=" <<  VDOUBLE(cache[sym.second.cache_idx],cache[sym.second.cache_idx+1],cache[sym.second.cache_idx+2]) << "; ";
	}
	return s.str();
}
