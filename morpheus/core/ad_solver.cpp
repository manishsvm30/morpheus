#include "ad_solver.h"
#include "fwd_euler_ad_solver.h"
#include "implicit_ad_solver.h"
#include <xtensor/xio.hpp>
// #include <xtensor/xnoalias.hpp>

std::shared_ptr<AD_Solver_I> AD_Solver_Base::createInstance(const AD_Descriptor& descr) {
	if (descr.ad_solver == AD_Solver::WellMixed)  {
		cout << "Creating WellMixedSolver"<< endl;
		return std::make_shared<WellMixedSolver>(descr);
	}
	else if (descr.ad_solver == AD_Solver::FwdEuler) {
		cout << "Creating FwdEulerADSolver"<< endl;
		return std::make_shared<FwdEulerADSolver>(descr);
	}
	else if (descr.ad_solver == AD_Solver::Implicit) {
		cout << "Creating ImplicitADSolver"<< endl;
		return std::make_shared<ImplicitADSolver>(descr);
	}
	return std::shared_ptr<AD_Solver_I>();
}

void AD_Solver_Base::setADBoundaries(AD_Descriptor::scalar_tensor *x0) const {
	const VINT s_origin = VINT(0,0,0); /// origin of the shadow region 
	const VINT i_origin = d.origin;
	const VINT shadow_width = d.origin;
	const VINT i_top = d.top;
	const VINT s_top = d.top + shadow_width ;           /// top of the shadow region
	auto& data = *x0;
	
	// On hexagonal lattice there are 2 neihgbors at a boundary
	if (  false /*lattice.getStructure() == Lattice::hexagonal_new*/) 
	{}
	else {
		if (shadow_width.x != 1) {
			throw (string("AD_Solver_Base::setADBoundaries Missing implementation for shadown width " + to_str(shadow_width.x)));
		}
		auto xmb = xt::view(data, xt::all(), xt::all(), s_origin.x);
		auto xpb = xt::view(data, xt::all(), xt::all(), s_top.x);
		if (d.boundary_types[Boundary::mx] == Boundary::periodic) {
			xmb = xt::view(data, xt::all(), xt::all(), i_top.x);
			xpb = xt::view(data, xt::all(), xt::all(), i_origin.x);
		}
		else {
			if (d.boundary_values[Boundary::mx]->isSpaceConst()) {
				xmb = d.boundary_values[Boundary::mx]->get(VINT());
			}
			else {
				// manually iterate over VINT positions
				VINT start = s_origin;
				VINT stop  = s_top+1;
				stop.x = i_origin.x;
				VINT pos = start;
				
				for (; pos.z<stop.z; pos.z++)
					for (pos.y=start.y; pos.y<stop.y; pos.y++)
						for (pos.x=start.x; pos.x<stop.x; pos.x++) {
							data(pos.z,pos.y,pos.x) = d.boundary_values[Boundary::mx]->get(pos-shadow_width);
						}
			}
			if (d.boundary_values[Boundary::px]->isSpaceConst()) {
				xpb = d.boundary_values[Boundary::px]->get(VINT());
			}
			else {
				// manually iterate over VINT positions
				VINT start = s_origin;
				VINT stop  = s_top+1;
				start.x = s_top.x; 
				VINT pos = start;
				
				for (; pos.z<stop.z; pos.z++)
					for (pos.y=start.y; pos.y<stop.y; pos.y++)
						for (pos.x=start.x; pos.x<stop.x; pos.x++) {
							data(pos.z,pos.y,pos.x) = d.boundary_values[Boundary::px]->get(pos-shadow_width);
						}
			}
		}
		
		if (shadow_width.y>0) {
			auto ymb = xt::view(data, xt::all(), s_origin.y, xt::all());
			auto ypb = xt::view(data, xt::all(), s_top.y,    xt::all());
			if (d.boundary_types[Boundary::my] == Boundary::periodic) {
				xt::view(data, xt::all(), s_origin.y, xt::all()) = xt::view(data, xt::all(), i_top.y, xt::all());
				xt::view(data, xt::all(), s_top.y,    xt::all()) = xt::view(data, xt::all(), i_origin.y, xt::all());
			}
			else {
				if (d.boundary_values[Boundary::my]->isSpaceConst()) {
					ymb = d.boundary_values[Boundary::my]->get(VINT());
				}
				else {
					// manually iterate over VINT positions
					VINT start = s_origin;
					VINT stop  = s_top+1;
					stop.y = i_origin.y;
					VINT pos = start;
					for (; pos.z<stop.z; pos.z++)
						for (pos.y=start.y; pos.y<stop.y; pos.y++)
							for (pos.x=start.x; pos.x<stop.x; pos.x++) {
								data(pos.z,pos.y,pos.x) = d.boundary_values[Boundary::my]->get(pos-shadow_width);
							}
				}
				if (d.boundary_values[Boundary::py]->isSpaceConst()) {
					ypb = d.boundary_values[Boundary::py]->get(VINT());
				}
				else {
					// manually iterate over VINT positions
					VINT start = s_origin;
					VINT stop  = s_top+1;
					start.y = s_top.y;
					VINT pos = start;
					for (; pos.z<stop.z; pos.z++) {
						for (pos.y=start.y; pos.y<stop.y; pos.y++) {
							for (pos.x=start.x; pos.x<stop.x; pos.x++) {
								data(pos.z,pos.y,pos.x) = d.boundary_values[Boundary::py]->get(pos-shadow_width);
							}
						}
					}
				}
			}
		}
		
		if (shadow_width.z>0) {
			auto zmb = xt::view(data, s_origin.z, xt::all(), xt::all());
			auto zpb = xt::view(data, s_top.z,    xt::all(), xt::all());
			if (d.boundary_types[Boundary::mz] == Boundary::periodic) {
				zmb = xt::view(data, i_top.z, xt::all(), xt::all());
				zpb = xt::view(data, i_origin.z, xt::all(), xt::all());
			}
			else {
				if (d.boundary_values[Boundary::mz]->isSpaceConst()) {
					zmb = d.boundary_values[Boundary::mz]->get(VINT());
				}
				else {
					// manually iterate over VINT positions
					VINT start = s_origin;
					VINT stop  = s_top+1;
					stop.z = i_origin.z;
					VINT pos = start;
					for (; pos.z<stop.z; pos.z++)
						for (pos.y=start.y; pos.y<stop.y; pos.y++)
							for (pos.x=start.x; pos.x<stop.x; pos.x++) {
								data(pos.z,pos.y,pos.x) = d.boundary_values[Boundary::mz]->get(pos-shadow_width);
							}
				}
				if (d.boundary_values[Boundary::pz]->isSpaceConst()) {
					zpb = d.boundary_values[Boundary::pz]->get(VINT());
				}
				else {
					// manually iterate over VINT positions
					VINT start = s_origin;
					VINT stop  = s_top+1;
					start.z = s_top.z;
					VINT pos = start;
					for (; pos.z<stop.z; pos.z++)
						for (pos.y=start.y; pos.y<stop.y; pos.y++)
							for (pos.x=start.x; pos.x<stop.x; pos.x++) {
								data(pos.z,pos.y,pos.x) = d.boundary_values[Boundary::pz]->get(pos-shadow_width);
							}
				}
			}
		}
	}
}

