//////
//
// This file is part of the modelling and simulation framework 'Morpheus',
// and is made available under the terms of the BSD 3-clause license (see LICENSE
// file that comes with the distribution or https://opensource.org/licenses/BSD-3-Clause).
//
// Authors:  Joern Starruss and Walter de Back
// Copyright 2009-2016, Technische Universität Dresden, Germany
//
//////

/*

This file provides a Monte Carlo Sampler that remembers all edges between nodes of unequal states.

*/


#ifndef EDGE_LIST
 #define EDGE_LIST
 
#define OMP_MAX_THREADS 64

#include <list>
#include <functional>
#include <algorithm>
#include <limits>
#include <atomic>
#include <shared_mutex>
#include "utils/safe_ptr.h"
#include "cpm_layer.h"

// get access to the global randomness
extern uint getRandomUint(uint max);
extern bool getRandomBool();
// 	extern gsl_rng* random_engine;

inline unsigned long l_rand(unsigned long max_val) {
	return (rand() | rand() << 15) % max_val;
}


// Base class for all edge trackers that provide potential updates.
class EdgeTrackerBase {
protected:
	shared_ptr< const CPM::LAYER > cell_layer;
	shared_ptr< const Lattice> lattice;
	Neighborhood opx_neighbors;
	Neighborhood surface_neighbors;
public:
	EdgeTrackerBase(shared_ptr<const  CPM::LAYER > p, const Neighborhood& opx_nei, const Neighborhood& surface_nei) : cell_layer(p), lattice(p->getLattice()), opx_neighbors(opx_nei), surface_neighbors(surface_nei) {};
	virtual ~EdgeTrackerBase() {};
	/// Number of update trials in a MCS
	virtual uint updates_per_mcs() const  =0;
	/// Pick a random update
	virtual void get_update(VINT& origin, VINT& direction) const =0;
	/// Notify the stepper that an update has occured
	virtual void update_notifier(const VINT& pos, const LatticeStencil& neighborhood) =0;
	/// Reset the stepper, i.e. the cell configuration may have changed completely
	virtual void reset() =0;
	virtual bool has_surface(const VINT& pos) const =0;
	virtual uint n_surfaces(const VINT& pos) const =0;
	/// The update neighborhood used
	const Neighborhood& getNeighborhood() const { return opx_neighbors; }
	virtual string getStatInfo() const {return "no stats"; }
	virtual ostream& dump(ostream& out) const { return out; }
};

class NoEdgeTracker : public EdgeTrackerBase {
public:
	NoEdgeTracker(shared_ptr< const CPM::LAYER > p, const Neighborhood& opx_nei, const Neighborhood& surface_nei) : EdgeTrackerBase(p, opx_nei, surface_nei)
	{
		max_source = this->lattice->size() - VINT(1,1,1);
		min_source = VINT(0,0,0);
		// Increase the size in case of constant boundaries
		if (cell_layer->getBoundaryType(Boundary::mx) != Boundary::noflux) min_source.x -= 1;
		if (cell_layer->getBoundaryType(Boundary::my) != Boundary::noflux) min_source.y -= 1;
		if (cell_layer->getBoundaryType(Boundary::mz) != Boundary::noflux) min_source.z -= 1;
		
		if (cell_layer->getBoundaryType(Boundary::px) != Boundary::noflux) max_source.x += 1;
		if (cell_layer->getBoundaryType(Boundary::py) != Boundary::noflux) max_source.y += 1;
		if (cell_layer->getBoundaryType(Boundary::pz) != Boundary::noflux) max_source.z += 1;
		
	};
	
	uint updates_per_mcs() const override {
		VINT a = this->cell_layer->size();
		return a.x * a.y * a.z;
	};
	
	void get_update(VINT& origin, VINT& direction) const override {
		// Just pick a random position and a random neighbor
		while(1) {
			VINT s = max_source-min_source;
			origin.x = getRandomUint(s.x);
			origin.y = getRandomUint(s.y);
			origin.z = (s.z>1) ? getRandomUint(s.z) : 0; 
			
			origin += min_source;
			
			const auto& neighbors = this->opx_neighbors.neighbors(origin);
			direction = neighbors[ getRandomUint(neighbors.size()-1) ];
			if (! this->cell_layer->writable(origin+direction)) continue;
			break;
		}
	}
	// Nothing to do on lattice updates ...
	void update_notifier(const VINT& pos, const LatticeStencil& neighborhood) override {};
	
	void reset() override {};
	
	bool has_surface(const VINT& pos) const override {
		const auto& spin = cell_layer->get(pos);
		// check the boundary neighborhood
		for (const auto& offset : this->surface_neighbors.neighbors(pos)) {
			if (spin != cell_layer->get(pos+offset) )
				return true;
		}
		return false;
	}
	
	uint n_surfaces(const VINT& pos) const override {
		const auto& spin = this->cell_layer->get(pos);
		// check 1st order neighborhood
		uint n_edges=0;
		for (const auto& offset : this->surface_neighbors.neighbors(pos)) {
			if (spin != cell_layer->get(pos+offset) )
				n_edges++;
		}
		return n_edges;
	}
	
private:
	VINT min_source, max_source;
};

template <class T>
class constWrite : public T {
public:
	typename T::value_type& operator[](int index) const {
		return const_cast<constWrite<T>*>(this)->operator[](index);
	}
};


class EdgeListTracker : public EdgeTrackerBase {
private:
	typedef unsigned int Edge_Id;
// 	typedef vector<unsigned int> Edge_Id_List;  // the edge id container per node, does not occupy memory when empty
	typedef Edge_Id* Edge_Id_List;  // the edge id container per node, does not occupy memory when empty
	 
	static const unsigned int no_edge;
	static const unsigned int no_flux_edge;

	struct Edge {
			VINT pos;
			unsigned short direction;
			bool valid = false;
	} ;

	deque<Edge> edges;
	std::atomic<deque<Edge>::size_type> edges_size;
	mutable CPM::mutex edges_wrt_mtx;
	mutable sf::contention_free_shared_mutex<OMP_MAX_THREADS> edges_shared_mtx;
// 	mutable std::shared_mutex edges_shared_mtx;
	
	struct alignas(hardware_destructive_interference_size) WorkerData {
		 vector<Edge_Id> edge_id_stash;
		 vector<Edge_Id_List> edge_list_stash;
	};
	
	static thread_local WorkerData worker_data;
	
	vector< vector<Edge_Id>* > thread_edge_stash;
	unique_ptr< Lattice_Data_Layer<Edge_Id_List> > edge_lattice;
	
	const int edge_list_size;
	
	bool is_alternate;
	vector<unsigned short> inverse_neighbor;
	vector<unsigned short> inverse_alternate_neighbor;
	vector<bool> opx_is_surface;
	
	
	void init_edge_list();
	
	std::vector< EdgeListTracker::Edge_Id_List >& edgeListStash();
	std::vector<EdgeListTracker::Edge_Id>& edgeStash();
	
	void defragment();
	void wipe();

public:
	EdgeListTracker(shared_ptr< const CPM::LAYER > p, const Neighborhood& opx_nei, const Neighborhood& surface_nei);
	~EdgeListTracker();
	EdgeListTracker(const EdgeListTracker& other) = delete;
	const EdgeListTracker& operator=(const EdgeListTracker& other) = delete;
	uint updates_per_mcs() const override;
	void get_update(VINT& origin, VINT& direction) const noexcept override;
	void update_notifier(const VINT& pos, const LatticeStencil& neighborhood) override;
	void tryDefragment() const;
	void reset() override;
	
	bool has_surface(const VINT& pos) const override;
	uint n_surfaces(const VINT& pos) const override;
	string getStatInfo() const override;
	ostream& dump(ostream& out) const override;

};



#endif // EDGE_LIST




