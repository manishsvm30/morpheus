#ifndef CPMSHAPE_H
#define CPMSHAPE_H

#include "vec.h"
#include "lattice.h"
#include "cpm_layer.h"


/** 
 * \brief CPMShape keeps the basic logics of a CPMShape 
 * 
 * 
 * */

struct EllipsoidShape{
	vector< VDOUBLE > axes;
	vector< double> lengths;
	double eccentricity;
	VDOUBLE center;
	uint volume;
	double timestamp;
};

// inherently sorted deque

class NodeStorage_deque : public deque<VINT> {
public:
	using deque<VINT>::insert;
	iterator insert(const VINT& val) {
		auto pos = lower_bound(begin(),end(),val, less_VINT());
		return deque<VINT>::insert(pos, val);
	};
	using  deque<VINT>::erase;
	bool erase(const VINT& val) {
		auto pos = lower_bound(begin(),end(),val, less_VINT());
		if (*pos == val) {
			deque<VINT>::erase(pos);
			return true;
		}
		else
			return false;
	};
	void sort();
};

class NodeStorage_vector : public vector<VINT> {
public:
	iterator insert(const VINT& val) {
		auto pos = lower_bound(begin(),end(),val, less_VINT());
		return vector<VINT>::insert(pos, val);
	};
	bool erase(const VINT& val) {
		auto pos = lower_bound(begin(),end(),val, less_VINT());
		if (*pos == val) {
			vector<VINT>::erase(pos);
			return true;
		}
		else
			return false;
	};
	void sort();
};

class NodeStorage_set : public set<VINT,less_VINT> {
public:
	void push_back(const VINT& val) { insert(val); };
	void sort() {};
};


class CPMShape
{
public:
	typedef NodeStorage_deque Nodes;
	enum class BoundaryScalingMode {Magno, NeigborNumber, None};
	static double BoundaryLengthScaling(const Neighborhood& neighborhood);
	static const Neighborhood& boundaryNeighborhood() { return boundary_neighborhood; }
	static void setBoundaryNeighborhood(const Neighborhood& neighborhood) {
		cout << "Setting boundary neighborhood to order " << neighborhood.order() << endl;
		boundary_neighborhood = neighborhood;
	}
	static BoundaryScalingMode scalingMode;
private:
	static Neighborhood boundary_neighborhood;
};




/** CPM shape tracker atop of an external node storage **/

// class CPMShapeTracker {
// public:
// 	CPMShapeTracker(const vector<VINT>& nodes );
// 	
// 	// modifiers
// 	void addNode(const VINT& node);
// 	void removeNode(const VINT& node);
// 	void moveNode(const VINT& node, const VINT& new_node);
// 	void copyUpdate(const CPMShapeTracker& other);
// 	
// 	// accessors
// 	const set<VINT> nodes();
// 	/// center in orthogonal coordinates
// 	const VDOUBLE center() const;
// 	/// center in lattice coordinates
// 	const VDOUBLE centerLatt() const;
// 	/// perimeter length with respect to the boundary neighborhood selected
// 	double perimeter() const;
// 	const map<CPM::CELL_ID, double>& InterfaceLengths() const;
// 	
// 	const EllipsoidShape& ellipsoid() const;
// 	
// 	/// Surface nodes. All nodes at the boundary with respect to a neighborhood of
// 	/// distance sqrt(2)
// 	const set<VINT>& surface() const;  // boundary nodes
// 	
// 	
// 	 	
// private:
// 	mutable EllipsoidShape elipsoid;
// 	bool valid_elipsoid;
// };

#endif // CPMSHAPE_H
