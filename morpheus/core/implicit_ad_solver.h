#ifndef IMPLICIT_AD_SOLVER_H
#define IMPLICIT_AD_SOLVER_H

#include "fwd_euler_ad_solver.h"

class ImplicitADSolver : public AD_Solver_Base {
public:
	ImplicitADSolver(const AD_Descriptor &d) : AD_Solver_Base(d) {};
	
	void setNodeLength(double value) override {};
	double maxTimeStepHint() const override { return std::numeric_limits<double>::max(); };
	int computeTimeStep(SymbolFocus focus, double time_step, AD_Descriptor::scalar_tensor *x0, AD_Descriptor::scalar_tensor *x1) override { return 0; };
// 	void writeSolution() const override {};
	
};

#endif
