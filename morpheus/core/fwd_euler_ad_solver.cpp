//////
//
// This file is part of the modelling and simulation framework 'Morpheus',
// and is made available under the terms of the BSD 3-clause license (see LICENSE
// file that comes with the distribution or https://opensource.org/licenses/BSD-3-Clause).
//
// Authors:  Joern Starruss and Walter de Back
// Copyright 2009-2016, Technische Universität Dresden, Germany
//
//////

#include "fwd_euler_ad_solver.h"
#include "xtensor/xindex_view.hpp"
#include "xtensor/xoperation.hpp"
#include "xtensor/xnoalias.hpp"
#include "focusrange.h"

int WellMixedSolver::computeTimeStep(SymbolFocus focus, double time_step, AD_Descriptor::scalar_tensor *x0, AD_Descriptor::scalar_tensor *x1) 
{
	if (d.field_type == AD_Descriptor::FIELD) {
		double effective_flux = 0;
		// TODO does not respect boundary fluxes.
		
		if (d.has_domain_and_mask) {
			double average_concentration = xt::sum(xt::filter(*x0, xt::equal(*d.boundary_mask, Boundary::none)))();
			filtration(*x1,xt::equal(*d.boundary_mask, Boundary::none)) = average_concentration;
			for (auto type : d.boundary_types) {
				if (type == Boundary::flux)
					throw string("WellMixedSolver: Missing implementation for flux boundaries in domains.");
			}
		}
		else {
			auto origin = d.origin;
			auto top = d.top+1;
			double vol = (top.x - origin.x) * (top.y-origin.y) * (top.z-origin.z);
			
			double average_concentration = xt::mean(xt::view(*x0,xt::range(origin.z,top.z),xt::range(origin.y,top.y),xt::range(origin.x,top.x)))();
// 			cout << "Average value " << d.origin << "-" << d.top << " = " << average_concentration << endl;
			if (d.boundary_types[Boundary::mx] == Boundary::flux) {
				average_concentration += xt::sum(xt::view(*x0,xt::range(origin.z,top.z), xt::range(origin.y,top.y), origin.x-1))() * time_step / vol;
			}
			if (d.boundary_types[Boundary::px] == Boundary::flux) {
				average_concentration += xt::sum(xt::view(*x0,xt::range(origin.z,top.z), xt::range(origin.y,top.y), top.x))() * time_step / vol;
			}
			if (d.boundary_types[Boundary::my] == Boundary::flux) {
				average_concentration += xt::sum(xt::view(*x0,xt::range(origin.z,top.z), origin.y-1, xt::range(origin.x,top.x)))() * time_step / vol;
			}
			if (d.boundary_types[Boundary::py] == Boundary::flux) {
				average_concentration += xt::sum(xt::view(*x0,xt::range(origin.z,top.z), top.y, xt::range(origin.x,top.x)))() * time_step / vol;
			}
			if (d.boundary_types[Boundary::mz] == Boundary::flux) {
				average_concentration += xt::sum(xt::view(*x0,origin.z-1, xt::range(origin.y,top.y), xt::range(origin.x,top.x)))() * time_step / vol;
			}
			if (d.boundary_types[Boundary::pz] == Boundary::flux) {
				average_concentration += xt::sum(xt::view(*x0,top.z, xt::range(origin.y,top.y), xt::range(origin.x,top.x)))() * time_step / vol;
			}

			x1->fill( average_concentration );
		}
	}
	else if (d.field_type == AD_Descriptor::MEMBRANE) {
		const auto& node_sizes = MembranePropertyPlugin::nodeSizes();
		double sum = 0; double size = 0;
		for (int y=0; y<d.size.y;y++ ){
			sum  += node_sizes[y] * xt::sum(xt::view(*x0, 0, y, xt::range(d.origin.x,d.top.x+1)))();
			size += node_sizes[y] * d.size.x;
		}
		x1->fill( sum/size );
	}
	return 0; 
}
	
int FwdEulerADSolver::computeTimeStep(SymbolFocus focus, double time_step, AD_Descriptor::scalar_tensor *x0, AD_Descriptor::scalar_tensor *x1) 
{
	// Algorithm to automatically refine the time steps if necessary
	double solved_time  = 0;
	double partial_delta_t = time_step;
	double my_node_length =0;
	const int dimensions = lattice->getDimensions();
	const bool report_solver=false;
	
	using SolverMethod =  bool (FwdEulerADSolver::*)(double time_interval, double node_length, AD_Descriptor::scalar_tensor* diffusion,  AD_Descriptor::scalar_tensor *x0, AD_Descriptor::scalar_tensor *x1) ; 
		SolverMethod solver;

	AD_Descriptor::scalar_tensor diffusion_cache;
	double diffusion_max=0;
	
	// compute the node length
	if ( d.field_type == AD_Descriptor::MEMBRANE ) {
		double physical_node_length = SIM::getNodeLength();
		double equator_circumference;

		if (dimensions == 1) {
			equator_circumference =  sqrt(4.0 * M_PI * focus.cell().getSize());
			my_node_length = (equator_circumference * physical_node_length) / MembraneProperty::getResolution();
			if (node_length< my_node_length) node_length = my_node_length;
		}
		else if (dimensions == 2) {
			equator_circumference = pow(6.0 * sqr(M_PI) * focus.cell().getSize(), (1.0/3.0));
			my_node_length = (equator_circumference * physical_node_length) / MembraneProperty::getResolution();
			if (node_length< my_node_length) node_length = my_node_length;
		}
	}
	else {
		my_node_length = static_pointer_cast<const Field::Symbol>(d.field_symbol)->getField()->getNodeLength();
	}

	// compute diffusion cache and select solver
	if (!d.diffusion->flags().space_const || d.has_domain_and_mask ) {
		diffusion_cache = xt::zeros<double>(x1->shape());
		if ( d.field_type == AD_Descriptor::MEMBRANE ) {
			if (dimensions == 2) {
				if (report_solver) cout << "Selecting spheric solver " << endl;
				solver = & FwdEulerADSolver::solve_fwd_euler_diffusion_spheric;
			}
			else {
				if (report_solver) cout << "Selecting heterogeneous solver " << endl;
				solver = & FwdEulerADSolver::solve_fwd_euler_diffusion_heterogeneous;
			}
			FocusRange range(Granularity::MembraneNode, focus.cellID());
			for (const auto& f : range ) {
				auto diffusion = d.diffusion->get(f);
				diffusion_max = diffusion>diffusion_max ? diffusion : diffusion_max;
				const auto& p = d.origin + f.membrane_pos();
				diffusion_cache(p.z,p.y,p.x) = diffusion; 
			}
			xt::view(diffusion_cache, xt::all(), xt::all(), d.origin.x-1) = xt::view(diffusion_cache, xt::all(), xt::all(), d.top.x);
			xt::view(diffusion_cache, xt::all(), xt::all(), d.top.x+1) = xt::view(diffusion_cache, xt::all(), xt::all(), d.origin.x);
		}
		else {
			if (report_solver) cout << "Selecting heterogeneous solver " << endl;
			solver = & FwdEulerADSolver::solve_fwd_euler_diffusion_heterogeneous;
			my_node_length = static_pointer_cast<const Field::Symbol>(d.field_symbol)->getField()->getNodeLength();
			// This also respects domain shapes
			FocusRange range(Granularity::Node, SIM::getGlobalScope());
			for (const auto& f : range ) {
				auto diffusion = d.diffusion->get(f);
				diffusion_max = diffusion>diffusion_max ? diffusion : diffusion_max;
				const auto& p = d.origin + f.pos();
				diffusion_cache(p.z,p.y,p.x) = diffusion;
			}
			if (d.boundary_types[Boundary::px] == Boundary::periodic) {
				xt::view(diffusion_cache, xt::all(), xt::all(), d.origin.x-1) = xt::view(diffusion_cache, xt::all(), xt::all(), d.top.x);
				xt::view(diffusion_cache, xt::all(), xt::all(), d.top.x+1) = xt::view(diffusion_cache, xt::all(), xt::all(), d.origin.x);
			}
			if (d.boundary_types[Boundary::py] == Boundary::periodic) {
				xt::view(diffusion_cache, xt::all(), d.origin.y-1, xt::all()) = xt::view(diffusion_cache, xt::all(), d.top.y, xt::all());
				xt::view(diffusion_cache, xt::all(), d.top.y+1, xt::all()) = xt::view(diffusion_cache, xt::all(), d.origin.y, xt::all());
			}
			if (d.boundary_types[Boundary::pz] == Boundary::periodic) {
				xt::view(diffusion_cache, d.origin.z-1, xt::all(), xt::all()) = xt::view(diffusion_cache, d.top.z, xt::all(), xt::all());
				xt::view(diffusion_cache, d.top.z+1, xt::all(), xt::all()) = xt::view(diffusion_cache, d.origin.z, xt::all(), xt::all());
			}
		}
	}
	else {
		diffusion_max = d.diffusion->get(focus);
		diffusion_cache = {{{ diffusion_max }}};
		// Select the 
		if ( d.field_type == AD_Descriptor::MEMBRANE ) {
			if (focus.cell().getSize()==0) {
				if (report_solver) cout << "Selecting well-mixed solver " << endl;
				solver = & FwdEulerADSolver::solve_well_mixed;
			}
			else if (dimensions == 1) {
				if (report_solver) cout << "Selecting homogeneous solver " << endl;
				solver = & FwdEulerADSolver::solve_fwd_euler_diffusion_homogeneous;
			}
			else if (dimensions == 2) {
				if (report_solver) cout << "Selecting spheric solver " << endl;
				solver = & FwdEulerADSolver::solve_fwd_euler_diffusion_spheric;
			}
		}
		else {
			if (report_solver) cout << "Selecting homogeneous solver " << endl;
			solver = & FwdEulerADSolver::solve_fwd_euler_diffusion_homogeneous;
		}
	}
	
	
	
	bool has_const_boundaries = false;
	for (auto bdry : d.boundary_types) {
		if (bdry == Boundary::constant) has_const_boundaries = true;
	}
	int step_count = ceil( (dimensions * 2.0 + has_const_boundaries) * diffusion_max / sqr(my_node_length) * time_step * 1.05 );
	partial_delta_t = time_step / step_count;
// 	cout << " dt = " << partial_delta_t << endl;
	
	while (1) {
		double dt = min(partial_delta_t,time_step-solved_time);
		if ((this->*solver)(dt, my_node_length, &diffusion_cache, x0, x1)) {
			// Apply the discrete interpolation step be forwarding time into write buffer x1
			solved_time += dt;
		} else {
			// Refine the step width in order to not break the stability criterion. This branch is actually stale since we precompute a
			// suitable partial_delta_t
			partial_delta_t  *= 0.5;
// 			cout << setprecision(8) << "Refined diffusion step to " << partial_delta_t << " due to nl " << my_node_length << " D " << d.diffusion->get(focus) << endl;
			continue;
		}
		if (solved_time + 10e-16 >= time_step) {
			break;
		}
		else {
			std::swap(*x0,*x1);
			
			// reset boundaries ...
			if (d.boundary_types[Boundary::px] == Boundary::periodic) {
				xt::view(*x0, xt::all(), xt::all(), d.origin.x-1) = xt::view(*x0, xt::all(), xt::all(), d.top.x);
				xt::view(*x0, xt::all(), xt::all(), d.top.x+1) = xt::view(*x0, xt::all(), xt::all(), d.origin.x);
			}
			else {
				xt::view(*x0, xt::all(), xt::all(), d.origin.x-1) = xt::view(*x1, xt::all(), xt::all(), d.origin.x-1);
				xt::view(*x0, xt::all(), xt::all(), d.top.x+1) = xt::view(*x1, xt::all(), xt::all(), d.top.x+1);
			}
			if (dimensions >= 2) {
				if (d.boundary_types[Boundary::py] == Boundary::periodic) {
					xt::view(*x0, xt::all(), d.origin.y-1, xt::all()) = xt::view(*x0, xt::all(), d.top.y, xt::all());
					xt::view(*x0, xt::all(), d.top.y+1, xt::all()) = xt::view(*x0, xt::all(), d.origin.y, xt::all());
				}
				else if (d.size.y>1) {
					xt::view(*x0, xt::all(), d.origin.y-1, xt::all()) = xt::view(*x1, xt::all(), d.origin.y-1, xt::all());
					xt::view(*x0, xt::all(), d.top.y+1, xt::all()) = xt::view(*x1, xt::all(), d.top.y+1, xt::all());
				}
			}
			if (dimensions >= 3) {
				if (d.boundary_types[Boundary::pz] == Boundary::periodic) {
					xt::view(*x0, d.origin.z-1, xt::all(), xt::all()) = xt::view(*x0, d.top.z, xt::all(), xt::all());
					xt::view(*x0, d.top.z+1, xt::all(), xt::all()) = xt::view(*x0, d.origin.z, xt::all(), xt::all());
				}
				else if (d.size.z>1) {
					xt::view(*x0, d.origin.z-1, xt::all(), xt::all()) = xt::view(*x1, d.origin.z-1, xt::all(), xt::all());
					xt::view(*x0, d.top.z+1, xt::all(), xt::all()) = xt::view(*x1, d.top.z+1, xt::all(), xt::all());
				}
			}
		}
		
	}
	return 0; 
	
};


bool FwdEulerADSolver::solve_well_mixed(double time_step, double node_length, AD_Descriptor::scalar_tensor* diffusion, AD_Descriptor::scalar_tensor *x0, AD_Descriptor::scalar_tensor *x1) {
	double average_concentration = xt::sum(xt::view(*x0,xt::range(d.origin.z,d.top.z+1),xt::range(d.origin.y,d.top.y+1),xt::range(d.origin.x,d.top.x+1)))();
// 	cout << "Average value " << d.origin << "-" << d.top << " = " << average_concentration << endl;
	auto origin = d.origin;
	auto top = d.top+1;
	auto vol = (top.z-origin.z)*(top.y-origin.y)*(top.x-origin.x);
	VDOUBLE flux_scale(1,1,1);
	if (lattice->getStructure() == Lattice::hexagonal || lattice->getStructure() == Lattice::hexagonal_new) {
		flux_scale.x *= M_HALF_SQRT3;
	}
	flux_scale*=time_step / vol;
	
	if (d.boundary_types[Boundary::mx] == Boundary::flux) {
		average_concentration += xt::sum(xt::view(*x0,xt::range(origin.z,top.z), xt::range(origin.y,top.y), origin.x-1))() * flux_scale.x;
	}
	if (d.boundary_types[Boundary::px] == Boundary::flux) {
		average_concentration += xt::sum(xt::view(*x0,xt::range(origin.z,top.z), xt::range(origin.y,top.y), top.x))() * flux_scale.x;
	}
	if (d.boundary_types[Boundary::my] == Boundary::flux) {
		average_concentration += xt::sum(xt::view(*x0,xt::range(origin.z,top.z), origin.y-1, xt::range(origin.x,top.x)))() * flux_scale.y;
	}
	if (d.boundary_types[Boundary::py] == Boundary::flux) {
		average_concentration += xt::sum(xt::view(*x0,xt::range(origin.z,top.z), top.y, xt::range(origin.x,top.x)))() * flux_scale.y;
	}
	if (d.boundary_types[Boundary::mz] == Boundary::flux) {
		average_concentration += xt::sum(xt::view(*x0,origin.z-1, xt::range(origin.y,top.y), xt::range(origin.x,top.x)))() * flux_scale.z;
	}
	if (d.boundary_types[Boundary::pz] == Boundary::flux) {
		average_concentration += xt::sum(xt::view(*x0,top.z, xt::range(origin.y,top.y), xt::range(origin.x,top.x)))() * flux_scale.z;
	}
	x1->fill( average_concentration / ((d.top.z+1 - d.origin.z)*(d.top.y+1 - d.origin.y)*(d.top.x+1 - d.origin.x)) );
	return true;
}

bool FwdEulerADSolver::solve_fwd_euler_diffusion_homogeneous(double time_interval, double node_length, AD_Descriptor::scalar_tensor* diffusion,  AD_Descriptor::scalar_tensor *x0, AD_Descriptor::scalar_tensor *x1)
{
	// normalisation ...
// 	double node_flux = (*diffusion)(0,0,0) * sqr(node_length);
	double alpha = time_interval * (*diffusion)(0,0,0) / sqr(node_length);

	// numerical stability threshold
	auto& data = *x0;
	auto& write_buffer = *x1;
	auto origin = d.origin;
	auto top = d.top+1;
	// row major layout x,y,z
	VINT shadow_offset(1,x0->shape(2), x0->shape(2)*x0->shape(1));
	vector<vector<VINT>> n_list { Neighborhood_1.neighbors() };
	bool is_hex = false;
	
	const double beta_critical = 0.0;
	if (lattice->getStructure() == Lattice::linear ) {
		double beta = (1.0-2*alpha);
		// numerical stability criterion
		if (beta <= beta_critical) return false;
		xt::noalias(xt::view(write_buffer, 0,0,xt::range(origin.x,top.x))) =
			beta * xt::view(data, 0,0,xt::range(origin.x,top.x))
			+ alpha * (
				xt::view(data, 0,0,xt::range(origin.x-1,top.x-1))
				+ xt::view(data, 0,0,xt::range(origin.x+1,top.x+1))
			);
	}
	else if (lattice->getStructure() == Lattice::square )  {
		double beta = (1.0-4*alpha);
		// numerical stability criterion
		if (beta < beta_critical) return false;
// 		// The xtensor implementation
		auto r =  xt::view(write_buffer, 0, xt::range(origin.y,top.y),     xt::range(origin.x,top.x));
		auto d =  xt::view(data,         0, xt::range(origin.y,top.y),     xt::range(origin.x,top.x));
		auto n0 = xt::view(data,         0, xt::range(origin.y,top.y), xt::range(origin.x-1,top.x-1));
		auto n1 = xt::view(data,         0, xt::range(origin.y-1,top.y-1), xt::range(origin.x,top.x));
		auto n2 = xt::view(data,         0, xt::range(origin.y,top.y), xt::range(origin.x+1,top.x+1));
		auto n3 = xt::view(data,         0, xt::range(origin.y+1,top.y+1), xt::range(origin.x,top.x));
		xt::noalias(r) = beta * d + alpha*(n0+n1+n2+n3);
		
// 		auto ii_write_buffer = &write_buffer(0,0,0); //xt::flatten(write_buffer);
// 		auto ii_data = &data(0,0,0); // xt::flatten(data);
// 
// 		int n0_off = dot(VINT(-1, 0,0), shadow_offset);
// 		int n1_off = dot(VINT( 1, 0,0), shadow_offset);
// 		int n2_off = dot(VINT( 0,-1,0), shadow_offset);
// 		int n3_off = dot(VINT( 0, 1,0), shadow_offset);
// // 		cout << "origin " <<  d.origin << " top " << d.top  << endl;
// // 		cout << "Shadow offset " <<  shadow_offset << " start " << start << " stop " << stop  << endl;
// #pragma omp parallel for schedule(static)
// 		for (int y=origin.y; y<top.y; y++ ) {
// 			int start  = dot(VINT(origin.x,y,0), shadow_offset); /** fist lattice index **/
// 			int stop   = dot(VINT(top.x,y,0), shadow_offset); // last lattice index
// 			for (int idx=start; idx<stop; idx++ ) {
// 				ii_write_buffer[idx] = 
// 					beta * ii_data[idx]
// 					+ alpha * (
// 						ii_data[idx + n0_off]
// 						+ ii_data[idx + n1_off]
// 						+ ii_data[idx + n2_off]
// 						+ ii_data[idx + n3_off]
// 					);
// 			}
// 		}
	} 
	else if (lattice->getStructure() == Lattice::hexagonal )  {
// 		(alpha * dimensions / (neighbors/2)) = alpha *2*dimensions/neighbors
		alpha *= 2.0*2.0/6.0; // rescale from 4 to 6 2d-neighbors
		double beta = (1.0-6*alpha);
		// numerical stability criterion
		if (beta < beta_critical) return false;
		
		const auto& neighbors = Neighborhood_1.neighbors();
		VINT n;
		auto r =  xt::view(write_buffer, 0, xt::range(origin.y,top.y),     xt::range(origin.x,top.x));
		auto d =  xt::view(data,         0, xt::range(origin.y,top.y),     xt::range(origin.x,top.x));
		n=neighbors[0]; auto n0 = xt::view(data,0, xt::range(origin.y+n.y,top.y+n.y), xt::range(origin.x+n.x,top.x+n.x));
		n=neighbors[1]; auto n1 = xt::view(data,0, xt::range(origin.y+n.y,top.y+n.y), xt::range(origin.x+n.x,top.x+n.x));
		n=neighbors[2]; auto n2 = xt::view(data,0, xt::range(origin.y+n.y,top.y+n.y), xt::range(origin.x+n.x,top.x+n.x));
		n=neighbors[3]; auto n3 = xt::view(data,0, xt::range(origin.y+n.y,top.y+n.y), xt::range(origin.x+n.x,top.x+n.x));
		n=neighbors[4]; auto n4 = xt::view(data,0, xt::range(origin.y+n.y,top.y+n.y), xt::range(origin.x+n.x,top.x+n.x));
		n=neighbors[5]; auto n5 = xt::view(data,0, xt::range(origin.y+n.y,top.y+n.y), xt::range(origin.x+n.x,top.x+n.x));
		xt::noalias(r) = beta * d + alpha*(n0+n1+n2+n3+n4+n5);

		is_hex = true;
	}
	else if (lattice->getStructure() == Lattice::hexagonal_new )  {
// 		(alpha * dimensions / (neighbors/2)) = alpha *2*dimensions/neighbors
		
		alpha *= 2.0*2.0/6.0; // rescale from 4 to 6 2d-neighbors
		double beta = (1.0-6*alpha);
		// numerical stability criterion
		if (beta < beta_critical) return false;

		// The xtensor implementation
		{
			const auto& neighbors = Neighborhood_1.neighbors();
			VINT n;
			auto r =  xt::view(write_buffer, 0, xt::range(origin.y,top.y,2),     xt::range(origin.x,top.x));
			auto d =  xt::view(data,         0, xt::range(origin.y,top.y,2),     xt::range(origin.x,top.x));
			n=neighbors[0]; auto n0 = xt::view(data,0, xt::range(origin.y+n.y,top.y+n.y,2), xt::range(origin.x+n.x,top.x+n.x));
			n=neighbors[1]; auto n1 = xt::view(data,0, xt::range(origin.y+n.y,top.y+n.y,2), xt::range(origin.x+n.x,top.x+n.x));
			n=neighbors[2]; auto n2 = xt::view(data,0, xt::range(origin.y+n.y,top.y+n.y,2), xt::range(origin.x+n.x,top.x+n.x));
			n=neighbors[3]; auto n3 = xt::view(data,0, xt::range(origin.y+n.y,top.y+n.y,2), xt::range(origin.x+n.x,top.x+n.x));
			n=neighbors[4]; auto n4 = xt::view(data,0, xt::range(origin.y+n.y,top.y+n.y,2), xt::range(origin.x+n.x,top.x+n.x));
			n=neighbors[5]; auto n5 = xt::view(data,0, xt::range(origin.y+n.y,top.y+n.y,2), xt::range(origin.x+n.x,top.x+n.x));
			xt::noalias(r) = beta * d + alpha*(n0+n1+n2+n3+n4+n5);
		}
		origin.y ++;
		{
			const auto& neighbors = Neighborhood_1.alternating_neighbors();
			VINT n;
			auto r =  xt::view(write_buffer, 0, xt::range(origin.y,top.y,2),     xt::range(origin.x,top.x));
			auto d =  xt::view(data,         0, xt::range(origin.y,top.y,2),     xt::range(origin.x,top.x));
			n=neighbors[0]; auto n0 = xt::view(data,0, xt::range(origin.y+n.y,top.y+n.y,2), xt::range(origin.x+n.x,top.x+n.x));
			n=neighbors[1]; auto n1 = xt::view(data,0, xt::range(origin.y+n.y,top.y+n.y,2), xt::range(origin.x+n.x,top.x+n.x));
			n=neighbors[2]; auto n2 = xt::view(data,0, xt::range(origin.y+n.y,top.y+n.y,2), xt::range(origin.x+n.x,top.x+n.x));
			n=neighbors[3]; auto n3 = xt::view(data,0, xt::range(origin.y+n.y,top.y+n.y,2), xt::range(origin.x+n.x,top.x+n.x));
			n=neighbors[4]; auto n4 = xt::view(data,0, xt::range(origin.y+n.y,top.y+n.y,2), xt::range(origin.x+n.x,top.x+n.x));
			n=neighbors[5]; auto n5 = xt::view(data,0, xt::range(origin.y+n.y,top.y+n.y,2), xt::range(origin.x+n.x,top.x+n.x));
			xt::noalias(r) = beta * d + alpha*(n0+n1+n2+n3+n4+n5);
		}
		
		is_hex = true;
		// we have alternating neighbors
		n_list.push_back(Neighborhood_1.alternating_neighbors());
	} 
	else if ( lattice->getStructure() == Lattice::cubic ) {
		double beta = (1.0-6*alpha);
		// numerical stability criterion
		if (beta < beta_critical) return false;
		
		{
			const auto& neighbors = Neighborhood_1.neighbors();
			VINT n;
			auto r =  xt::view(write_buffer, xt::range(origin.z,top.z), xt::range(origin.y,top.y),     xt::range(origin.x,top.x));
			auto d =  xt::view(data,         xt::range(origin.z,top.z), xt::range(origin.y,top.y),     xt::range(origin.x,top.x));
			n=neighbors[0]; auto n0 = xt::view(data, xt::range(origin.z+n.z,top.z+n.z), xt::range(origin.y+n.y,top.y+n.y), xt::range(origin.x+n.x,top.x+n.x));
			n=neighbors[1]; auto n1 = xt::view(data, xt::range(origin.z+n.z,top.z+n.z), xt::range(origin.y+n.y,top.y+n.y), xt::range(origin.x+n.x,top.x+n.x));
			n=neighbors[2]; auto n2 = xt::view(data, xt::range(origin.z+n.z,top.z+n.z), xt::range(origin.y+n.y,top.y+n.y), xt::range(origin.x+n.x,top.x+n.x));
			n=neighbors[3]; auto n3 = xt::view(data, xt::range(origin.z+n.z,top.z+n.z), xt::range(origin.y+n.y,top.y+n.y), xt::range(origin.x+n.x,top.x+n.x));
			n=neighbors[4]; auto n4 = xt::view(data, xt::range(origin.z+n.z,top.z+n.z), xt::range(origin.y+n.y,top.y+n.y), xt::range(origin.x+n.x,top.x+n.x));
			n=neighbors[5]; auto n5 = xt::view(data, xt::range(origin.z+n.z,top.z+n.z), xt::range(origin.y+n.y,top.y+n.y), xt::range(origin.x+n.x,top.x+n.x));
			xt::noalias(r) = beta * d + alpha*(n0+n1+n2+n3+n4+n5);
		}
	}
	else {
		cerr << "no implementation !! " << endl; assert(0); exit(-1);
	}
	
// 	// alpha --> the correct diffusive flux scaling of a single neighbor
// 	auto d_scale = alpha;


	// const_scale - constant boundaries flux
	double const_scale = 2*alpha;
	// time * interface / volume_scaling
	double flux_scale = time_interval / node_length;
	if (is_hex) {
		// we have two boundary neighbors per site
// 		const_scale *= 1;  
		flux_scale  *= 0.5;
		// Volume of nodes
		flux_scale  /= M_HALF_SQRT3;
	}
	// fix miscalculation of const flux boundaries and restore boundaries
	int qy=n_list.size();
	for ( int i_list = 0; i_list<n_list.size(); i_list++) {
		const auto& neighbors = n_list[i_list];
		const auto iy=i_list;
		for (uint i=0; i<neighbors.size(); i++) {
			VINT lo = d.origin;
			lo.y += i_list;
			VINT hi = d.top+1;
			if (neighbors[i].x != 0 && d.boundary_types[Boundary::mx] != Boundary::periodic) {
				int bdry;
				Boundary::Type bdry_type;
				if (neighbors[i].x > 0) {
					bdry = hi.x-1;
					bdry_type = d.boundary_types[Boundary::px];
					hi.x-=neighbors[i].x;
				}
				else /*if (neighbors[i].x < 0)*/ {
					bdry = lo.x;
					bdry_type = d.boundary_types[Boundary::mx];
					lo.x-=neighbors[i].x;
				}
				auto lo_n = lo + neighbors[i];
				auto hi_n = hi + neighbors[i];
				
				// compensate
				xt::view(*x1, xt::range(lo.z,hi.z), xt::range(lo.y,hi.y, qy), bdry) += - alpha *
							( xt::view(*x0, xt::range(lo_n.z,hi_n.z), xt::range(lo_n.y,hi_n.y, qy), bdry+neighbors[i].x)
								- xt::view(*x0, xt::range(lo.z,hi.z), xt::range(lo.y,hi.y, qy), bdry)
							);
				
				auto l_lo = lo;
				auto l_hi = hi;
				bool first_row = d.origin.y == lo.y;
				l_lo.y += (neighbors[i].y<0 && first_row ? qy :0 );
				l_hi.y -= (neighbors[i].y>0 && (hi.y-lo.y)%qy ==1 ? qy :0 );
				lo_n = l_lo + neighbors[i]; 
				hi_n = l_hi + neighbors[i]; 
				
				if (bdry_type == Boundary::constant) {
					auto s = const_scale;
					if (neighbors[i].y !=0) {
						xt::view(*x1, xt::all(), xt::range(l_lo.y,l_hi.y, qy), bdry) +=
								s * ( 0.5 * xt::view(*x0, xt::all(), xt::range(lo_n.y,hi_n.y, qy), bdry+neighbors[i].x)
									+ 0.5 * xt::view(*x0, xt::all(), xt::range(l_lo.y,l_hi.y, qy), bdry+neighbors[i].x)
									- xt::view(*x0, xt::all(), xt::range(l_lo.y,l_hi.y, qy), bdry)
								);
					}
					else {
						xt::view(*x1, xt::all(), xt::range(l_lo.y,l_hi.y, qy), bdry) +=
								s * ( xt::view(*x0, xt::all(), xt::range(lo_n.y,hi_n.y, qy), bdry+neighbors[i].x)
									- xt::view(*x0, xt::all(), xt::range(l_lo.y,l_hi.y, qy), bdry)
								);
					}
				}
				else if (bdry_type == Boundary::flux || bdry_type == Boundary::noflux) {
					double f = flux_scale;
					// interface length scaling
					if (is_hex) {
						f*= (neighbors[i].y==0 ? 1.0 : 0.5) / M_HALF_SQRT3;
					}
					xt::view(*x1, xt::range(l_lo.z,l_hi.z), xt::range(l_lo.y,l_hi.y, qy), bdry) += 
						xt::view(*x0, xt::range(lo_n.z,hi_n.z), xt::range(lo_n.y,hi_n.y, qy), bdry+neighbors[i].x) * f;
				}
			}
			if (neighbors[i].y != 0 && d.boundary_types[Boundary::my] != Boundary::periodic) {
				int bdry = -1;
				Boundary::Type bdry_type;
				if (neighbors[i].y > 0 && (qy==1 || (((hi.y-lo.y)%qy)==1)) ) {
					bdry = hi.y-1;
					bdry_type = d.boundary_types[Boundary::py];
					hi.y-=qy;
				}
				else if (neighbors[i].y < 0 && (qy==1 || iy==0)) {
					bdry = lo.y;
					bdry_type = d.boundary_types[Boundary::my];
					lo.y+=qy;
				}
				if (bdry>=0) {
					auto lo_n = lo + neighbors[i];
					auto hi_n = hi + neighbors[i];
					if (bdry_type == Boundary::constant) {
						xt::view(*x1, xt::range(lo.z,hi.z), bdry, xt::range(lo.x,hi.x)) += (const_scale - alpha) 
								* ( xt::view(*x0, xt::range(lo_n.z,hi_n.z), bdry+neighbors[i].y, xt::range(lo_n.x,hi_n.x))
									- xt::view(*x0, xt::range(lo.z,hi.z), bdry, xt::range(lo.x,hi.x))
								);
					}
					else if (bdry_type == Boundary::flux || bdry_type == Boundary::noflux) {
						xt::view(*x1, xt::range(lo.z,hi.z), bdry, xt::range(lo.x,hi.x)) += 
								xt::view(*x0, xt::range(lo_n.z,hi_n.z), bdry+neighbors[i].y, xt::range(lo_n.x,hi_n.x)) * flux_scale
								- alpha *
								( xt::view(*x0, xt::range(lo_n.z,hi_n.z), bdry+neighbors[i].y, xt::range(lo_n.x,hi_n.x))
									- xt::view(*x0, xt::range(lo.z,hi.z), bdry, xt::range(lo.x,hi.x))
								);
					}
				}
			}
			if (neighbors[i].z != 0 && d.boundary_types[Boundary::mz] != Boundary::periodic) {
				int bdry;
				Boundary::Type bdry_type;
				if (neighbors[i].z > 0) {
					bdry = hi.z-1;
					bdry_type = d.boundary_types[Boundary::pz];
				}
				else /*if (neighbors[i].x < 0)*/ {
					bdry = lo.z;
					bdry_type = d.boundary_types[Boundary::mz];
				}
				auto lo_n = lo + neighbors[i];
				auto hi_n = hi + neighbors[i];
				if (bdry_type == Boundary::constant) {
					xt::view(*x1, bdry, xt::range(lo.y,hi.y), xt::range(lo.x,hi.x)) += (const_scale - alpha) *
							( xt::view(*x0,  bdry+neighbors[i].z, xt::range(lo_n.y,hi_n.y), xt::range(lo_n.x,hi_n.x))
								- xt::view(*x0, bdry, xt::range(lo.y,hi.y), xt::range(lo.x,hi.x))
							);
				}
				else if (bdry_type == Boundary::flux || bdry_type == Boundary::noflux) {
					xt::view(*x1, bdry, xt::range(lo.y,hi.y), xt::range(lo.x,hi.x)) += 
							xt::view(*x0, bdry+neighbors[i].z, xt::range(lo_n.y,hi_n.y), xt::range(lo_n.x,hi_n.x)) * flux_scale
							- alpha * 
							( xt::view(*x0,  bdry+neighbors[i].z, xt::range(lo_n.y,hi_n.y), xt::range(lo_n.x,hi_n.x))
								- xt::view(*x0, bdry, xt::range(lo.y,hi.y), xt::range(lo.x,hi.x))
							);
				}
			}
		}
	}
	
	return true;
}

template <class T> 
auto sub_view(T& cont, VINT lo, VINT hi, VINT n = VINT(0,0,0)) {
	lo+=n; hi+=n;
	return xt::view( cont, xt::range(lo.z, hi.z), xt::range(lo.y, hi.y), xt::range(lo.x, hi.x));
}

template <class T> 
auto sub_view(T& cont, VINT lo, VINT hi, int stride, VINT n = VINT(0,0,0)) {
	lo+=n; hi+=n;
	return xt::view( cont, xt::range(lo.z, hi.z), xt::range(lo.y, hi.y, stride), xt::range(lo.x, hi.x) );
}

template <class T> 
auto sub_view_2d(T& cont, VINT lo, VINT hi, int stride, VINT n = VINT(0,0,0)) {
	lo+=n; hi+=n;
	return xt::view( cont, 0, xt::range(lo.y, hi.y, stride), xt::range(lo.x, hi.x) );
}

bool FwdEulerADSolver::solve_fwd_euler_diffusion_heterogeneous(double time_interval, double node_length, AD_Descriptor::scalar_tensor *diffusion_rate, AD_Descriptor::scalar_tensor *x0, AD_Descriptor::scalar_tensor *x1) {
	
	auto neigborhood = lattice->getNeighborhoodByOrder(1);
	const vector<VINT>& neighbors = neigborhood.neighbors();
	const vector<VINT>& a_neighbors = neigborhood.alternating_neighbors();

	const double alpha_scale = 2.0 * double(lattice->getDimensions()) / neighbors.size();
	
	const auto dim = lattice->getDimensions();
	const auto d_scale = alpha_scale * time_interval / (node_length * node_length);
	double const_scale = 2;
	
	if (d.has_domain_and_mask) {
		VINT shadow_offset(1,x0->shape(2), x0->shape(2)*x0->shape(1));
		VINT shadow_width = d.origin;
		auto lo = d.origin; auto hi = d.top+1;
		
		int outer_size = (hi.z-lo.z) *(hi.y-lo.y);
		int inner_size = (hi.x-lo.x);
	
		auto x = xt::flatten(*x0);
		auto b = xt::flatten(*d.boundary_mask);
		auto d = xt::flatten(*diffusion_rate);
		auto f = xt::flatten(*x1);
		
		vector<int> n_offset;
		for (auto n : neighbors) {
			n_offset.push_back(dot(n,shadow_offset));
		}
		vector<int> an_offset;
		for (auto n : a_neighbors) {
			an_offset.push_back(dot(n,shadow_offset));
		}
		
		double f_scale = time_interval;
		if (dim==2 && neighbors.size() == 6) {
			f_scale*=0.5;
			const_scale*=0.5;
		}
		
#pragma omp parallel for schedule(static)
		for (int outer=0; outer < outer_size; outer++ ) {
			VINT position = VINT(0,outer%(hi.y-lo.y), outer/(hi.y-lo.y));
			int start = dot(lo + position, shadow_offset);
			int stop = start+inner_size;
			const auto& offset = !an_offset.empty() && (position.y & 1) ? an_offset : n_offset;
			for (int idx=start; idx<=stop; idx++ ) {
				f[idx] = x[idx];
				if (b[idx] == Boundary::none) {
					for (uint i=0; i<neighbors.size(); i++) {
						auto nidx = idx+offset[i];
						f[idx] += (b[nidx] == Boundary::none) ? 2*d[idx]*d[nidx]/(d[idx]+d[nidx]+1e-16)*(x[nidx] - x[idx]) * d_scale :
								  (b[nidx] == Boundary::flux) ? x[nidx] * f_scale :
								  (b[nidx] == Boundary::noflux) ? 0.0 : const_scale*d[idx]*(x[nidx] - x[idx]) * d_scale ;
					}
				}
			}
		}
	}
	else {
		if (dim==1) {
			VINT lo = d.origin;
			VINT hi = d.top+1;
				
			auto d_c = sub_view(*diffusion_rate, lo, hi);
			auto x =   sub_view(*x0,             lo, hi);
			auto f =   sub_view(*x1,             lo, hi);
			
			auto neighbor_flux = [&](int idx) {
				return sub_view(*diffusion_rate, lo, hi, neighbors[idx]) / (d_c+sub_view(*diffusion_rate, lo, hi, neighbors[idx]) + 1e-16) * ( sub_view(*x0, lo, hi, neighbors[idx]) - x);
			};
			auto s = 2*d_scale;
			xt::noalias(f) = x + s*d_c *( neighbor_flux(0) + neighbor_flux(1) );
			
			// time * interface / volume_scaling
			double f_scale = time_interval / node_length;
			for (uint i=0; i<neighbors.size(); i++) {
				if (neighbors[i].x != 0 && d.boundary_types[Boundary::mx] != Boundary::periodic) {
					int bdry;
					Boundary::Type bdry_type;
					if (neighbors[i].x > 0) {
						bdry = hi.x-1;
						bdry_type = d.boundary_types[Boundary::px];
					}
					else /*if (neighbors[i].x < 0)*/ {
						bdry = lo.x;
						bdry_type = d.boundary_types[Boundary::mx];
					}
					if (bdry_type == Boundary::constant) {
						(*x1)(0,0,bdry) += (*diffusion_rate)(0,0,bdry) *((*x0)(0,0,bdry+neighbors[i].x)-(*x0)(0,0,bdry)) *  const_scale * d_scale;
					}
					else if (bdry_type == Boundary::flux) {
						(*x1)(0,0,bdry) += (*x0)(0,0,bdry+neighbors[i].x) * f_scale;
					}
				}
			}
		}
		else if (dim==2) {
			std::vector< vector<VINT> > n_list { neighbors };
			if (!a_neighbors.empty()) n_list.push_back(a_neighbors);

			const int qy = n_list.size();
			VINT lo = d.origin;
			const VINT hi = d.top+1;
			const auto s = 2*d_scale;
			// computing the core
			for (auto neighbors : n_list) {
				auto d_c = sub_view_2d(*diffusion_rate, lo, hi, qy);
				auto x =   sub_view_2d(*x0, lo, hi, qy);
				auto f =   sub_view_2d(*x1, lo, hi, qy);
				
				auto neighbor_flux = [&](int idx) {
					return sub_view_2d(*diffusion_rate, lo, hi, qy, neighbors[idx]) / (d_c+sub_view_2d(*diffusion_rate, lo, hi, qy, neighbors[idx]) + 1e-16) * ( sub_view_2d(*x0, lo, hi, qy, neighbors[idx]) - x);
				};

				if (neighbors.size()==6) {
					xt::noalias(f) = x + s*d_c *( neighbor_flux(0) + neighbor_flux(1) + neighbor_flux(2) + neighbor_flux(3) + neighbor_flux(4) + neighbor_flux(5) );
				}
				else {
					xt::noalias(f) = x + s*d_c *( neighbor_flux(0) + neighbor_flux(1) + neighbor_flux(2) + neighbor_flux(3) );
				}
				lo.y++;
			}
			// computing the boundaries
			VDOUBLE f_scale(1,1,1);
			VDOUBLE c_scale(1,1,1);
			if (neighbors.size()==6) {
// 				c_scale.y *= 0.5;
// 				c_scale.x *= 0.5;
				// Both neighbors have a 0.5 normal length
				f_scale.y *= 0.5;
				// The orth neigbor, other have 0.5 of that
				f_scale.x *= 1.0/M_SQRT3;
				// volume of nodes
				f_scale *= 1.0/M_HALF_SQRT3;
			}
			// time * interface / volume_scaling
			f_scale *= time_interval / node_length;
			c_scale *= const_scale;
				
			VINT origin = d.origin;
			for (auto neighbors : n_list) {
				for (uint i=0; i<neighbors.size(); i++) {
					VINT lo = origin;
					VINT hi = d.top+1;
				
					if (neighbors[i].x != 0 && d.boundary_types[Boundary::mx] != Boundary::periodic) {
						int bdry;
						Boundary::Type bdry_type;
						if (neighbors[i].x > 0) {
							bdry = hi.x-1;
							bdry_type = d.boundary_types[Boundary::px];
							hi.x-=neighbors[i].x;
						}
						else /*if (neighbors[i].x < 0)*/ {
							bdry = lo.x;
							bdry_type = d.boundary_types[Boundary::mx];
							lo.x-=neighbors[i].x;
						}
						auto l_lo = lo;
						auto l_hi = hi;
						bool first_row = d.origin.y == lo.y;
						l_lo.y += (neighbors[i].y<0 && first_row ? qy :0 );
						l_hi.y -= (neighbors[i].y>0 && (hi.y-lo.y)%qy ==1 ? qy :0 );
						auto lo_n = l_lo + neighbors[i]; 
						auto hi_n = l_hi + neighbors[i]; 
// 						lo_n.y = l_lo.y;hi_n.y = l_hi.y;
						if (bdry_type == Boundary::constant) {
							auto s = c_scale.x * d_scale;
							if (neighbors[i].y !=0) {
								xt::view(*x1, 0, xt::range(l_lo.y,l_hi.y, qy), bdry) += s *
										xt::view(*diffusion_rate, 0, xt::range(l_lo.y,l_hi.y, qy), bdry)
										* ( 0.5 * xt::view(*x0, 0, xt::range(lo_n.y,hi_n.y, qy), bdry+neighbors[i].x)
											+ 0.5 * xt::view(*x0, 0, xt::range(l_lo.y,l_hi.y, qy), bdry+neighbors[i].x)
											- xt::view(*x0, 0, xt::range(l_lo.y,l_hi.y, qy), bdry)
										);
							}
							else {
								xt::view(*x1, 0, xt::range(l_lo.y,l_hi.y, qy), bdry) += s *
										xt::view(*diffusion_rate, 0, xt::range(l_lo.y,l_hi.y, qy), bdry)
										* ( xt::view(*x0, 0, xt::range(lo_n.y,hi_n.y, qy), bdry+neighbors[i].x)
											- xt::view(*x0, 0, xt::range(l_lo.y,l_hi.y, qy), bdry)
										);
							}
						}
						else if (bdry_type == Boundary::flux) {
							auto s = f_scale.x * (neighbors[i].y == 0 ? 1.0 : 0.5);
							xt::view(*x1, 0, xt::range(l_lo.y,l_hi.y, qy), bdry) += xt::view(*x0, 0, xt::range(lo_n.y,hi_n.y, qy), bdry+neighbors[i].x) * s;
						}
					}
					if (neighbors[i].y != 0 && d.boundary_types[Boundary::my] != Boundary::periodic) {
						int bdry = -1;
						Boundary::Type bdry_type;
						if (neighbors[i].y > 0 && (qy==1 || ((hi.y-lo.y)%2)==1)) {
							bdry = hi.y-1;
							bdry_type = d.boundary_types[Boundary::py];
							hi.y-=qy;
						}
						else if (neighbors[i].y < 0 && (qy==1 || origin.y == d.origin.y)) {
							bdry = lo.y;
							bdry_type = d.boundary_types[Boundary::my];
							lo.y+=qy;
						}
						if (bdry>0) {
							auto lo_n = lo + neighbors[i];
							auto hi_n = hi + neighbors[i];
							if (bdry_type == Boundary::constant) {
								xt::view(*x1, 0, bdry, xt::range(lo.x,hi.x)) += const_scale *
										xt::view(*diffusion_rate, 0, bdry, xt::range(lo.x,hi.x))
										* ( xt::view(*x0, 0, bdry+neighbors[i].y, xt::range(lo_n.x,hi_n.x))
											- xt::view(*x0, 0, bdry, xt::range(lo.x,hi.x))
										) * d_scale;
							}
							else if (bdry_type == Boundary::flux) {
								xt::view(*x1, 0, bdry, xt::range(lo.x,hi.x)) += xt::view(*x0, 0, bdry+neighbors[i].y, xt::range(lo_n.x,hi_n.x)) * f_scale.y;
							}
						}
					}
				}
				origin.y++;
			}
		}

		else if (dim==3) {
			VINT lo = d.origin;
			VINT hi = d.top+1;
			
			auto d_c = sub_view(*diffusion_rate, lo, hi);
			auto x =   sub_view(*x0,             lo, hi);
			auto f =   sub_view(*x1,             lo, hi);
			
			auto neighbor_flux = [&](int idx) {
				return sub_view(*diffusion_rate, lo, hi, neighbors[idx]) / (d_c+sub_view(*diffusion_rate, lo, hi, neighbors[idx])  + 1e-16) * ( sub_view(*x0, lo, hi, neighbors[idx]) - x);
			};

			auto s = 2*d_scale;
			xt::noalias(f) = x + s*d_c *( neighbor_flux(0) + neighbor_flux(1) + neighbor_flux(2) + neighbor_flux(3) + neighbor_flux(4) + neighbor_flux(5) );
		
			// here we currently drop the ranges and rather use xt::all since all neighbors are axial.
			// if that changes, proper xt:range(lo_n.x,lo_n.y) must be used
			// time * interface / volume_scaling
			double f_scale = time_interval / node_length;
			for (uint i=0; i<neighbors.size(); i++) {
				if (neighbors[i].x != 0 && d.boundary_types[Boundary::mx] != Boundary::periodic) {
					int bdry;
					Boundary::Type bdry_type;
					if (neighbors[i].x > 0) {
						bdry = hi.x-1;
						bdry_type = d.boundary_types[Boundary::px];
					}
					else /*if (neighbors[i].x < 0)*/ {
						bdry = lo.x;
						bdry_type = d.boundary_types[Boundary::mx];
					}
					if (bdry_type == Boundary::constant) {
						xt::view(*x1, xt::all(), xt::all(), bdry) += const_scale *
								xt::view(*diffusion_rate, xt::all(), xt::all(), bdry)
								* ( xt::view(*x0, xt::all(), xt::all(), bdry+neighbors[i].x)
									- xt::view(*x0, xt::all(), xt::all(), bdry)
								) * d_scale;
					}
					else if (d.boundary_types[Boundary::px] == Boundary::flux) {
						xt::view(*x1, xt::all(), xt::all(), bdry) += xt::view(*x0, xt::all(), xt::all(), bdry+neighbors[i].x)*f_scale;
					}
				}
				if (neighbors[i].y != 0 && d.boundary_types[Boundary::my] != Boundary::periodic) {
					int bdry;
					Boundary::Type bdry_type;
					if (neighbors[i].y > 0) {
						bdry = hi.y-1;
						bdry_type = d.boundary_types[Boundary::py];
					}
					else /*if (neighbors[i].x < 0)*/ {
						bdry = lo.y;
						bdry_type = d.boundary_types[Boundary::my];
					}
					if (bdry_type == Boundary::constant) {
						xt::view(*x1, xt::all(), bdry, xt::all()) += const_scale *
								xt::view(*diffusion_rate, xt::all(), bdry, xt::all())
								* ( xt::view(*x0, xt::all(), bdry+neighbors[i].y, xt::all())
									- xt::view(*x0, xt::all(), bdry, xt::all())
								) * d_scale;
					}
					else if (bdry_type == Boundary::flux) {
						xt::view(*x1, xt::all(), bdry, xt::all()) += xt::view(*x0, xt::all(), bdry+neighbors[i].y, xt::all())*f_scale;
					}
				}
				if (neighbors[i].z != 0 && d.boundary_types[Boundary::mz] != Boundary::periodic) {
					int bdry;
					Boundary::Type bdry_type;
					if (neighbors[i].z > 0) {
						bdry = hi.z-1;
						bdry_type = d.boundary_types[Boundary::pz];
					}
					else /*if (neighbors[i].x < 0)*/ {
						bdry = lo.z;
						bdry_type = d.boundary_types[Boundary::mz];
					}
					if (bdry_type == Boundary::constant) {
						xt::view(*x1, bdry, xt::all(), xt::all()) += const_scale *
								xt::view(*diffusion_rate, bdry, xt::all(),  xt::all())
								* ( xt::view(*x0,  bdry+neighbors[i].z, xt::all(),xt::all())
									- xt::view(*x0, bdry, xt::all(), xt::all())
								) * d_scale;
					}
					else if (bdry_type == Boundary::flux) {
						xt::view(*x1, bdry, xt::all(), xt::all()) += xt::view(*x0, bdry+neighbors[i].z, xt::all(), xt::all())*f_scale;
					}
				}
			}
		}
	}
	return true;
}


bool FwdEulerADSolver::solve_fwd_euler_diffusion_spheric(double time_interval, double node_length, AD_Descriptor::scalar_tensor* diffusion,  AD_Descriptor::scalar_tensor *x0, AD_Descriptor::scalar_tensor *x1)
{
	omp_set_num_threads(1);
	// normalisation ... 
	bool homogeneous = (diffusion->shape(2)==1);
	double alpha = time_interval / sqr(node_length);
	double max_alpha = alpha/4 * (*diffusion)(0,0,0);
	
//	cout << "alpha = " << alpha << endl;
	
	if (homogeneous && max_alpha * lattice -> getNeighborhoodByOrder(1).size() >= 1.0 ){
		// ht < (hx^2 / 4D) (for the 2D lattice case)
//  		cout << "diffusion step in fwd euler spherical numerically unstable! " << alpha << endl;
//  		cout <<  " time_interval > node_length² / 2D" << endl;
//  		cout <<  time_interval << " > " << sqr(node_length) / (getLattice() -> getNeighborhood( 1 ).size() * diffusion_rate) << endl;
		return false;
	}
	
	// precalculated theta angles (angle to the x-y-plane)
	// Diffusion in phi
	auto theta_y = d.field_symbol->getField()->theta_y;
	auto phi_coarsening = d.field_symbol->getField()->phi_coarsening;

	xt::xtensor<double,1> alpha_phi = 
		alpha / xt::pow(xt::sin(theta_y) * (phi_coarsening),2);
	// Diffusion flux alpha in theta upwards
	xt::xtensor<double,1> alpha_theta_up = xt::zeros_like(alpha_phi);
	// Diffusion flux alpha in theta downwards
	xt::xtensor<double,1> alpha_theta_down = xt::zeros_like(alpha_phi); 
	
	for (uint i=0; i<alpha_theta_up.size(); i++) {
		// Diffusion in both half spheres, including compensation for differing node volumes
		if (theta_y[i] < M_PI/2) {
			alpha_theta_up[i] =    alpha;
			alpha_theta_down[i] =  alpha * (i==0 ? 0 : sin(theta_y[i-1])/sin(theta_y[i]));
		}
		else {
			alpha_theta_up[i] =    alpha * (i==theta_y.size()-1 ? 0 : sin(theta_y[i+1])/sin(theta_y[i]));
			alpha_theta_down[i] =  alpha;
		}
	}
	
	// X COARSENING
	VINT origin = d.origin;
	VINT size = d.top+1;
	VINT shadow_width = origin;
	auto& data =*x0;
	auto& write_buffer =*x1;
	VINT pos = origin;
	for (pos.y=origin.y; pos.y<size.y; pos.y++) {
// 		coarsening step in the data layer and phi resp. x diffusion
		auto lpos_y = pos.y - shadow_width.y;
		if (phi_coarsening[lpos_y] != 1) {
			// coarsening
			for (uint slice_start = origin.x; slice_start<size.x; slice_start += phi_coarsening[lpos_y] ) {
				// this is mass conserving redistribution
				const uint length = min(phi_coarsening[lpos_y], size.x-slice_start);
				auto slice = xt::view(data, 0, pos.y, xt::range(slice_start, slice_start+length));
				slice.fill(xt::sum(slice)() / slice.size());
			}
		}
	}
	
	// DIFFUSION
	setADBoundaries(x0);
	if (homogeneous) {
		double d_rate = (*diffusion)(0,0,0);
		for (pos.y=origin.y; pos.y<size.y; pos.y++) {
			// PHI resp. X DIFFUSION
			auto lpos_y = pos.y - shadow_width.y;
	// 		pos.x=0;
			double gamma = 1.0-(alpha_theta_up[lpos_y] + alpha_theta_down[lpos_y] + 2*alpha_phi[lpos_y]);

			if (phi_coarsening[lpos_y] == 1) {
				// diffusion
				xt::view(write_buffer, 0, pos.y, xt::range(origin.x, size.x)) = 
					gamma * xt::view(data, 0, pos.y, xt::range(origin.x, size.x))
					+ alpha_phi[lpos_y]        * xt::view(data, 0, pos.y, xt::range(origin.x-1, size.x-1))
					+ alpha_phi[lpos_y]        * xt::view(data, 0, pos.y, xt::range(origin.x+1, size.x+1))
					+ alpha_theta_up[lpos_y]   * xt::view(data, 0, pos.y+1, xt::range(origin.x, size.x))
					+ alpha_theta_down[lpos_y] * xt::view(data, 0, pos.y-1, xt::range(origin.x, size.x));
					
			}
			else if (phi_coarsening[lpos_y] == size.x-1) {
				//diffusion
				xt::view(write_buffer, 0, pos.y, xt::range(origin.x, size.x)) = 
					(gamma+2*alpha_phi[lpos_y]) * xt::view(data, 0, pos.y, xt::range(origin.x, size.x))
					+ alpha_theta_up[lpos_y]    * xt::view(data, 0, pos.y+1, xt::range(origin.x, size.x))
					+ alpha_theta_down[lpos_y]  * xt::view(data, 0, pos.y-1, xt::range(origin.x, size.x));
			}
			else {
				// diffusion
				for (uint slice = origin.x; slice<size.x; slice+=phi_coarsening[lpos_y] ) {
					uint length = min(phi_coarsening[lpos_y], size.x-slice);
					xt::view(write_buffer, 0, pos.y, xt::range(slice, slice+length)) =
						gamma * xt::view(data, 0, pos.y, xt::range(slice, slice+length))
						+ alpha_phi[lpos_y]        * ( data(0,pos.y,slice-1) + data(0,pos.y,slice+length))
						+ alpha_theta_up[lpos_y]   * xt::view(data, 0, pos.y+1, xt::range(slice, slice+length))
						+ alpha_theta_down[lpos_y] * xt::view(data, 0, pos.y-1, xt::range(slice, slice+length));
					
				}
			}
		}
	}
	else {
		const vector<VINT> neigbor = {{1,0,0},{-1,0,0},{0,1,0},{0,-1,0}};
		for (pos.y=origin.y; pos.y<size.y; pos.y++) {
			// PHI resp. X DIFFUSION
			auto lpos_y = pos.y - shadow_width.y;
	// 		pos.x=0;
			
			auto neighbor_flux = [&] (int lo, int hi, VINT n)  {
				return 2*xt::view(*diffusion, 0, pos.y+n.y, xt::range(lo-n.x, hi-n.x)) * xt::view(*diffusion, 0, pos.y, xt::range(lo, hi))/(xt::view(*diffusion, 0, pos.y+n.y, xt::range(lo-n.x, hi-n.x)) + xt::view(*diffusion, 0, pos.y, xt::range(lo, hi)))* (xt::view(data, 0, pos.y+n.y, xt::range(lo-n.x, hi-n.x)) - xt::view(data, 0, pos.y, xt::range(lo, hi)));
			};
			if (phi_coarsening[lpos_y] == 1) {
				// diffusion
				xt::view(write_buffer, 0, pos.y, xt::range(origin.x, size.x)) = 
					xt::view(data, 0, pos.y, xt::range(origin.x, size.x))
					+ alpha_phi[lpos_y]        * neighbor_flux(origin.x,size.x,neigbor[0])
					+ alpha_phi[lpos_y]        * neighbor_flux(origin.x,size.x,neigbor[1])
					+ alpha_theta_up[lpos_y]   * neighbor_flux(origin.x,size.x,neigbor[2])
					+ alpha_theta_down[lpos_y] * neighbor_flux(origin.x,size.x,neigbor[3]);
					
			}
			else if (phi_coarsening[lpos_y] == size.x-1) {
				//diffusion
				xt::view(write_buffer, 0, pos.y, xt::range(origin.x, size.x)) =
					xt::view(data, 0, pos.y, xt::range(origin.x, size.x))
					+ alpha_theta_up[lpos_y]   * neighbor_flux(origin.x,size.x,neigbor[2])
					+ alpha_theta_down[lpos_y] * neighbor_flux(origin.x,size.x,neigbor[3]);
			}
			else {
				// diffusion
				for (uint slice = origin.x; slice<size.x; slice+=phi_coarsening[lpos_y] ) {
					uint length = min(phi_coarsening[lpos_y], size.x-slice);
					xt::view(write_buffer, 0, pos.y, xt::range(slice, slice+length)) =
						xt::view(data, 0, pos.y, xt::range(slice, slice+length))
						+ alpha_phi[lpos_y]        * neighbor_flux(slice,slice+length,neigbor[0])
						+ alpha_phi[lpos_y]        * neighbor_flux(slice,slice+length,neigbor[1])
						+ alpha_theta_up[lpos_y]   * neighbor_flux(slice,slice+length,neigbor[2])
						+ alpha_theta_down[lpos_y] * neighbor_flux(slice,slice+length,neigbor[3]);
				}
			}
		}
	}

	return true;
}

