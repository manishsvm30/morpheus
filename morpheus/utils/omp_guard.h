#ifndef OMP_GUARD
#define OMP_GUARD

#include <omp.h>

class omp_mutex
{
public:
	omp_mutex()             {omp_init_lock(&_lock);}
	omp_mutex(const omp_mutex&) { omp_init_lock(&_lock); };
// 	omp_mutex& operator=(const omp_mutex&) { omp_init_lock(&_lock); return *this; };
	omp_mutex& operator=(const omp_mutex&) = delete;
	~omp_mutex()            {omp_destroy_lock(&_lock);}
	void lock()         {omp_set_lock(&_lock);}
	void unlock()           {omp_unset_lock(&_lock);}
	bool try_lock()      {return omp_test_lock(&_lock);}
private:
	omp_lock_t _lock;
	friend class omp_guard;
}; 



class omp_guard {
public:
	omp_guard(omp_lock_t* mtx) : _mtx(mtx) {  omp_set_lock(_mtx); }
	omp_guard(omp_mutex& mtx) : _mtx(&mtx._lock) {  omp_set_lock(_mtx); }
	~omp_guard() { omp_unset_lock(_mtx); }
	omp_guard (const omp_guard&) = delete;
    void operator= (const omp_guard&) = delete;
private :
	omp_lock_t* _mtx;
};


// omp shared mutex inspired by https://www.codeproject.com/Articles/1183423/We-Make-a-std-shared-mutex-10-Times-Faster


#endif
