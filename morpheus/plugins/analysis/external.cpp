#include "external.h"
#include <regex>

REGISTER_PLUGIN(External);

int External::instance_counter=0;

External::External() { 
	instance_id=instance_counter; instance_counter++;
	detach.setXMLPath("detach");
	detach.setDefault("true");
	registerPluginParameter( detach );
	
	timeout.setXMLPath("timeout");
	timeout.setDefault("30");
	registerPluginParameter( timeout );
	
	command_orig.setXMLPath("Command/text");
	registerPluginParameter( command_orig );
}
External::~External() { }


void External::loadFromXML(const XMLNode xNode, Scope* scope)
{
	AnalysisPlugin::loadFromXML(xNode, scope);

	if( command_orig().empty() )
		throw MorpheusException("External analysis plugin requires a (non-empty) command.", stored_node);
	
	for(uint i=0; i < xNode.nChildNode("Environment"); i++){
		XMLNode xEnvVar =  xNode.getChildNode("Environment",i);
		// get variable name and value from XML
		string variable, value;
		getXMLAttribute(xEnvVar, "variable", variable);
		getXMLAttribute(xEnvVar, "value", value);
		// store them in a map
		environvars[variable]=value; 
	}
}

void External::init(const Scope* scope)
{
	AnalysisPlugin::init(scope);
	
	// find Global symbols in command
	std::regex symbol_regex("(\\\\?)%([[:alpha:]]\\w*)?");
	std::smatch symbol_match;
	auto command = command_orig();
	size_t offset = 0;
	while (std::regex_search( command, symbol_match, symbol_regex) ) {
		Replacement r;
		r.pos = offset + symbol_match.position();
		if ( ( symbol_match[1].length() ) ) {
			// just remove the backlash
			r.len = 1;
			r.symbol = nullptr;
			replace_symbols.push_back(std::move(r));
		}
		else {
			if ( symbol_match.size() > 2) {
				// there was a symbol name match
				r.len = symbol_match.length();
				r.symbol = SIM::findGlobalSymbol<double>(symbol_match[2]);
				replace_symbols.push_back(std::move(r));
			}
		}
		offset += symbol_match.position()+symbol_match.length();
		command = symbol_match.suffix().str();
	}
	std::reverse(replace_symbols.begin(), replace_symbols.end());
}

void External::analyse(double time){
	execute();
}

void External::finish() {
	int unfinished = 0;
	for (auto d : detached_processes) {
		if (d->state == DetachedProcess::RUNNING) {
			unfinished++;
		}
	}
	
	int wait=0;
	cout << "\n";
	while (wait<timeout() && unfinished !=0) {
		cout << "External: Waiting " << setw(3) << timeout() - wait << " more seconds for " << unfinished << " unfinished processes \r";
		cout.flush();
		wait++;
		this_thread::sleep_for(chrono::seconds(1));

		unfinished=0;
		for (auto d : detached_processes) {
			if (d->state == DetachedProcess::RUNNING) {
				unfinished++;
			}
		}
	}
	cout << "\nExternal: There were " << unfinished << " running jobs at destruction." << endl;
	if (unfinished) cout << "External: Those are killed now." <<endl;
	
	for (auto d : detached_processes) {
		if (d->state == DetachedProcess::RUNNING) {
			d->process->kill();
			d->state = DetachedProcess::KILLED;
		}
	}
}

void External::execute(){
	string command = update_command(command_orig());
	
	string output_log = string("external_") + to_str(instance_id) + "_output.log";
	string error_log = string("external_") + to_str(instance_id) + "_error.log";
	for (auto& kv : environvars) {
#ifdef WIN32
		string def=kv.first +"="+kv.second;
		putenv(def.c_str());
#else
		setenv(kv.first.c_str(),kv.second.c_str(),1);
#endif
	}
	
	auto process = shared_ptr<TinyProcessLib::Process>(new 
		TinyProcessLib::Process( 
			command,
			"",
			[output_log](const char *bytes, size_t n) {
				ofstream fout;
				fout.open(output_log, std::ofstream::out | std::ofstream::app);
				fout.write(bytes, n);
				fout.close();
			},
			[error_log](const char *bytes, size_t n) {
				ofstream fout;
				fout.open(error_log, std::ofstream::out | std::ofstream::app);
				fout.write(bytes, n);
				fout.close();
			},
			true
		)
	);
	if (detach()) {
		cout << "External: running in detached mode"<<endl;
		auto det = shared_ptr<DetachedProcess>(new DetachedProcess());
		det->process = process;
		process.reset();
		det->state = DetachedProcess::RUNNING;
		detached_processes.push_back(det);
		thread thread_1([det]() {
			det->return_code = det->process->get_exit_status();
			det->state = DetachedProcess::FINISHED;
			det->process = nullptr;
			cout << "Posthoc analysis finished with return code: " << det->return_code << endl;
		});
		thread_1.detach();
	}
	else {
		// Wait for the child to exit.
		cout << "External: waiting for job to finish"<<endl;
		int returncode = process->get_exit_status();
		cout << "Posthoc analysis finished with return code: " << returncode << endl;
	}
}

string External::update_command(string command){
	
	for(const auto& r : replace_symbols){
		if (r.symbol) {
			string symbol_value;
			symbol_value= to_str(r.symbol->get(SymbolFocus::global));
			command.replace(r.pos, r.len, symbol_value);
		}
		else {
			command.replace(r.pos, r.len, "");
		}
	}
	return command;
}
